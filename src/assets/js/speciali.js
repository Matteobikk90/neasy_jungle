const quadriArray = [];
const clickedSizes = document.querySelectorAll(
  '.nj_categorie_section2 article'
);

fetch('https://neasy-jungle-1.herokuapp.com/offerteSpeciali')
  .then((response) => response.json())
  .then((quadriDetails) => {
    quadriArray.push(quadriDetails);
    // loader.classList.remove('active');
  })
  .catch((err) => console.log(err));

const handleSizeSection = (e) => {
  const size = e.currentTarget.dataset.attribute;
  const filteredItems = quadriArray[0].filter(
    (quadroArray) => quadroArray.size === size
  );

  console.log(filteredItems);
  let appendHtml = document.querySelector(`#${size} div`);
  filteredItems.map((filteredItem) => {
    const html = `
    <article>
    <a onclick="goToDetail(this.dataset.search)" href="#" data-search="${filteredItem.titolo}">
      <img src="${filteredItem.foto[0]}" />
        <h3>
          <span>${filteredItem.titolo}</span>
        </h3>
      <p>da &euro; ${filteredItem.prezzo[0]}</p>
    </a>
  </article>
                  `;
    appendHtml.innerHTML = html;
  });
};

clickedSizes.forEach((clickedSize) => {
  clickedSize.addEventListener('click', handleSizeSection);
});

// const t = document.getElementsByClassName('.goToDetailBtn');
// console.log(t);
// .forEach((goToDetailBtn) =>
//   goToDetailBtn.addEventListener('click', goToDetail)
// );

const goToDetail = (value) => {
  sessionStorage.setItem('detailProduct', value);
  window.location.href = '../html/dettaglio.html';
};
