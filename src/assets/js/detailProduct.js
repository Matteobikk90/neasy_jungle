const loader = document.querySelector('.loaderContainer');
const evidenzaCaorusel = document.querySelector(
  '.nj_evidenza_section2 .carousel-inner'
);
const footerDettaglio = document.querySelector('#footerDettaglio');
const evidenzaIndicators = document.querySelector('.nj_evidenza_section2 ul');
const productDetails = document.querySelector('.productDetails');
const dettaglioInfo = document.querySelector('.nj_dettalio_section3');
const prodottiCorrelati = document.querySelector('.nj_dettalio_section4');
const backBtn = document.querySelector('.backButton');
const randomSample = [];

loader.classList.add('active');

const handleBackBtn = () => {
  window.location = document.referrer;
};

backBtn.addEventListener('click', handleBackBtn);

if (sessionStorage.detailProduct) {
  fetch('https://neasy-jungle-1.herokuapp.com/quadriDetails')
    .then((response) => response.json())
    .then((quadriDetails) => {
      loader.classList.remove('active');
      const filteredProductDetail = quadriDetails.filter(
        (quadriDetail) => quadriDetail.titolo === sessionStorage.detailProduct
      );
      return filteredProductDetail;
    })
    .then((elements) => {
      elements[0].foto.map((foto, i) => {
        let htmlCarousel = '';
        let htmlIndicators = '';
        if (i === 0) {
          htmlCarousel = `
                            <div class="active item carousel-item" data-slide-number="${i}">
                              <img
                                src="${foto}"
                                class="img-fluid"
                              />
                            </div>`;
        } else {
          htmlCarousel = `
                            <div class="item carousel-item" data-slide-number="${i}">
                              <img
                                src="${foto}"
                                class="img-fluid"
                              />
                            </div>`;
        }

        if (i === 0) {
          htmlIndicators = `
                          <li class="list-inline-item active">
                            <a
                              id="carousel-selector-${i}"
                              class="selected"
                              data-slide-to="${i}"
                              data-target=".dettaglio"
                            >
                              <img src="${foto}" class="img-fluid" />
                            </a>
                          </li>
                              `;
        } else {
          htmlIndicators = `
                          <li class="list-inline-item">
                            <a
                              id="carousel-selector-${i}"
                              data-slide-to="${i}"
                              data-target=".dettaglio"
                            >
                              <img src="${foto}" class="img-fluid" />
                            </a>
                          </li>
                              `;
        }

        evidenzaCaorusel.innerHTML += htmlCarousel;
        evidenzaIndicators.innerHTML += htmlIndicators;
      });

      elements.map((element) => {
        const htmlDetailProduct = `
                          <h3><strong class="marked">${
                            element.titolo
                          }</strong></h3>
                          <input type="hidden" value="${
                            element.titolo
                          }" name="nomeTitolo" />
                          <p class="descrizioneprodotto">${
                            element.descrizione
                          }</p>
                          <p><strong>Spazio per:</strong> ${element.spazi}</p>
                          <p><strong>Piante nella foto:</strong> ${
                            element.piante
                          }</p>
                          ${
                            element.titolo.includes('capsule')
                              ? `<div class="capsule">
                              <p style="color: #8B0000">Seleziona il sistema compatibile:</p>
                              <input
                                type="radio"
                                id="nespresso"
                                name="capsule"
                                value="nespresso"
                                onclick="handleOpzioniObbligatorie(this.value)"
                              />
                              <label for="nespresso">Nespresso</label>
                                <input
                                type="radio"
                                id="lavazza"
                                name="capsule"
                                value="lavazza"
                                onclick="handleOpzioniObbligatorie(this.value)"
                              />
                              <label for="lavazza">Lavazza</label>
                            </div>`
                              : ''
                          }
                          ${
                            element.titolo.includes('lavagna')
                              ? `<div class="capsule">
                              <p style="color: #8B0000">Sei destro o mancino?</p>
                              <input
                                type="radio"
                                id="destro"
                                name="mano"
                                value="destro"
                                onclick="handleOpzioniObbligatorie(this.value)"
                              />
                              <label for="destro">Destro</label>
                                <input
                                type="radio"
                                id="mancino"
                                name="mano"
                                value="mancino"
                                onclick="handleOpzioniObbligatorie(this.value)"
                              />
                              <label for="mancino">Mancino</label>
                            </div>`
                              : ''
                          }
                          <button class="active pianteOpzioni" onclick="handlePrice('defaultPrice', ${
                            element.prezzo[0]
                          }, ${element.spedizioni[0]})">Senza piante</button>
                          <button class="pianteOpzioni" onclick="handlePrice('price', ${
                            element.prezzo[1]
                          }, ${element.spedizioni[1]})">Con piante</button>
                          <div class="senzaOpzioni">
                          <a style="margin: 10px 0 30px; display: block;" href="../html/video.html#comeInserirePianta">
                              <p class="reminder" style="color: #3b3b3b; line-height: 25px; "><span class="underlined">Clicca qui per scoprire quanto è facile inserire le tue piante preferite!</span></p>
                            </a>
                          </div>
                          <div class="opzioni">
                            <a style="margin: 10px; display: block;" href="../html/categorie.html">
                              <p class="reminder" style="color: #3b3b3b; line-height: 25px; "><span class="underlined">Hai dei dubbi nella scelta della pianta più adatta a te?</span></p>
                              <p class="reminder" style="color: #3b3b3b; line-height: 25px; "><span class="underlined">Clicca qui per scoprire le caratteristiche di ogni specie</span></p>
                            </a>
                            <p>Seleziona le piante:</p>
                            <div>
                            ${element.opzioni
                              .map(
                                (opzione, i) =>
                                  `<input
                                  type="radio"
                                  oninput="handleOpzioniPiante(this.value)"
                                  id="${opzione}"
                                  name="piante"
                                  value="${opzione}"
                                />
                                  <label for="${opzione}">${opzione}</label>
                                <br />`
                              )
                              .join(' ')}   
                              </div>
                          </div>
                          <p class="price">&euro; ${element.prezzo[0]}</p>
                          <p style="margin: 10px 0 20px">escluse spese di spedizione</p>
                             `;

        const footerDettaglioHtml = `
                             <h5>${element.titolo}</h5>
                             <form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
                            <input type="hidden" name="business" value="kin@kinskards.com">
                            <input type="hidden" name="cmd" value="_cart">
                            <input type="hidden" name="add" value="1">
                            <input type="hidden" name="item_name" value="${element.titolo}">
                            <input type="hidden" name="item_name_x"
value="Descrizione articolo">
<input type="hidden" name="item_number"
value="Numero articolo">
                            <input type="hidden" name="item_number" value="">
                            <input type="hidden" name="amount" value="${element.prezzo[0]}">
                            <input type="hidden" name="shipping" value="${element.spedizioni[0]}"/>
                            <input type="hidden" name="currency_code" value="EUR">
                            <input type="hidden" name="custom" value="Add special instructions to the seller:">
                            <input type="hidden" name="transaction_subject" value="Add special instructions to the seller:">
                            <input type="hidden" name="return" value="http://www.yourwebsite.com/ThankYou.html">
                            <input type="hidden" name="shopping_url" value="../html/prodotti.html" />
                            <input type="submit" value="Aggiungi al Carrello" name="submit" 
                              alt="Add to Cart">
                          </form>`;

        footerDettaglio.innerHTML += footerDettaglioHtml;

        if (element.titolo.includes('PLA')) {
          document.querySelector('.pvc').style.display = 'none';
          const htmlDetailInfo = `
          <section class="nj_homepage_section5" style="margin: 30px 15px">
                             <div class="accordion">
                               <a href="" class="main_category">
                               <svg
                               xmlns="http://www.w3.org/2000/svg"
                               height="50"
                               version="1.1"
                               viewBox="0 -51 512 512"
                               width="50"
                             >
                               <g>
                                 <g id="surface1">
                                   <path
                                     d="M 0 0 L 0 410 L 512 410 L 512 0 Z M 482 380 L 30 380 L 30 30 L 482 30 Z M 482 380 "
                                     data-original="#000000"
                                     class="active-path"
                                     data-old_color="#000000"
                                     fill="#3B3B3B"
                                   />
                                   <path
                                     d="M 180.375 310 L 121.214844 310 L 214.105469 217.105469 L 192.894531 195.894531 L 100 288.785156 L 100 229.625 L 70 229.625 L 70 340 L 180.375 340 Z M 180.375 310 "
                                     data-original="#000000"
                                     class="active-path"
                                     data-old_color="#000000"
                                     fill="#3B3B3B"
                                   />
                                   <path
                                     d="M 319.105469 214.105469 L 412 121.214844 L 412 180.375 L 442 180.375 L 442 70 L 331.625 70 L 331.625 100 L 390.785156 100 L 297.894531 192.894531 Z M 319.105469 214.105469 "
                                     data-original="#000000"
                                     class="active-path"
                                     data-old_color="#000000"
                                     fill="#3B3B3B"
                                   />
                                 </g>
                               </g>
                             </svg>                            
                                 <p>Dimensioni</p>
                                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background: new 0 0 512 512;" xml:space="preserve" width="20px" height="20px">
                                   <g>
                                     <g>
                                       <g>
                                         <path d="M492,236H276V20c0-11.046-8.954-20-20-20c-11.046,0-20,8.954-20,20v216H20c-11.046,0-20,8.954-20,20s8.954,20,20,20h216    v216c0,11.046,8.954,20,20,20s20-8.954,20-20V276h216c11.046,0,20-8.954,20-20C512,244.954,503.046,236,492,236z" data-original="#3b3b3b" class="active-path scroll" data-old_color="#3b3b3b" fill="$grafiteColor"></path>
                                       </g>
                                     </g>
                                   </g>
                                 </svg>
                               </a>
                               <div class="panel">
                                 <p>
                                   ${element.dimensioni}
                                 </p>
                               </div>
                               <a href="" class="main_category">
                               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 511.998 511.998" style="enable-background: new 0 0 511.998 511.998;" xml:space="preserve" width="50" height="50" class="">
              <g>
                <g>
                  <g>
                    <path d="M168.008,395.996h-8.524l38.938-38.938c2.86-2.86,3.715-7.161,2.167-10.898c-1.548-3.737-5.194-6.173-9.239-6.173h-65.665    c-5.523,0-10,4.477-10,10s4.477,10,10,10h41.523l-38.938,38.938c-2.86,2.86-3.715,7.161-2.167,10.898    c1.548,3.737,5.194,6.173,9.239,6.173h32.666c20.953,0,37.999,17.047,37.999,37.999s-17.047,37.999-37.999,37.999    s-37.999-17.047-37.999-37.999c0-5.523-4.477-10-10-10c-5.523,0-10,4.477-10,10c0,31.98,26.018,57.999,57.999,57.999    c31.98,0,57.999-26.018,57.999-57.999C226.007,422.014,199.989,395.996,168.008,395.996z" data-original="#3b3b3b" class="active-path scroll" fill="#3b3b3b"></path>
                  </g>
                </g>
                <g>
                  <g>
                    <path d="M290.015,339.997h-1.51c-5.523,0-10,4.477-10,10s4.477,10,10,10h1.51c28.677,0,52.008,23.331,52.008,52.009v27.979    c0,28.677-23.33,52.009-52.008,52.009h-23.99v-40.999c0-5.523-4.477-10-10-10c-5.523,0-10,4.477-10,10v50.999    c0,5.523,4.477,10,10,10h33.99c39.705,0,72.007-32.302,72.007-72.008v-27.979C362.023,372.299,329.72,339.997,290.015,339.997z" data-original="#3b3b3b" class="active-path scroll" fill="#3b3b3b"></path>
                  </g>
                </g>
                <g>
                  <g>
                    <path d="M263.096,407.925c-1.86-1.86-4.44-2.93-7.07-2.93s-5.21,1.07-7.07,2.93c-1.86,1.86-2.93,4.44-2.93,7.07    s1.07,5.21,2.93,7.07c1.86,1.86,4.44,2.93,7.07,2.93s5.21-1.07,7.07-2.93s2.93-4.44,2.93-7.07S264.956,409.785,263.096,407.925z" data-original="#3b3b3b" class="active-path scroll" fill="#3b3b3b"></path>
                  </g>
                </g>
                <g>
                  <g>
                    <path d="M502,40.004H377.997V10.005c0-5.523-4.477-10-10-10H144.003c-5.523,0-10,4.477-10,10v29.999H10c-5.523,0-10,4.477-10,10    v39.999c0,5.523,4.477,10,10,10h124.003v6c0,2.793,1.148,5.316,2.995,7.131l70.176,70.176c1.46,1.46,3.327,2.414,5.333,2.768    v37.906c0,3.519,1.85,6.779,4.871,8.584l28.729,17.166c-0.049,0.402-0.082,0.809-0.082,1.224v130.039c0,5.523,4.477,10,10,10    c5.523,0,10-4.477,10-10V250.958c0-0.422-0.035-0.836-0.086-1.245l28.694-17.145c3.021-1.805,4.871-5.065,4.871-8.584v-37.906    c2.006-0.354,3.874-1.308,5.333-2.768l70.236-70.235c-0.001-0.002-0.002-0.003-0.003-0.005c1.807-1.809,2.925-4.307,2.925-7.067    v-6h124.003c5.523,0,10-4.477,10-10V50.005C512,44.481,507.523,40.004,502,40.004z M134.003,80.003H20v-20h114.003V80.003z     M279.505,218.31l-23.499,14.042l-23.499-14.042v-32.071h46.999V218.31z M293.626,166.238h-75.239l-50.236-50.236h175.711    L293.626,166.238z M357.998,96.003H154.002V68.003h116.17c5.523,0,10-4.477,10-10s-4.477-10-10-10h-116.17V20.005h203.995V96.003z     M492,80.003L492,80.003H377.997v-20H492V80.003z" data-original="#3b3b3b" class="active-path scroll" fill="#3b3b3b"></path>
                  </g>
                </g>
                <g>
                  <g>
                    <path d="M312.69,48.004h-0.148c-5.523,0-10,4.477-10,10s4.477,10,10,10h0.148c5.523,0,10-4.477,10-10    S318.213,48.004,312.69,48.004z" data-original="#3b3b3b" class="active-path scroll" fill="#3b3b3b"></path>
                  </g>
                </g>
              </g>
            </svg>                          
                                 <p>Stampato in 3d</p>
                                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background: new 0 0 512 512;" xml:space="preserve" width="20px" height="20px">
                                   <g>
                                     <g>
                                       <g>
                                         <path d="M492,236H276V20c0-11.046-8.954-20-20-20c-11.046,0-20,8.954-20,20v216H20c-11.046,0-20,8.954-20,20s8.954,20,20,20h216    v216c0,11.046,8.954,20,20,20s20-8.954,20-20V276h216c11.046,0,20-8.954,20-20C512,244.954,503.046,236,492,236z" data-original="#3b3b3b" class="active-path scroll" data-old_color="#3b3b3b" fill="$grafiteColor"></path>
                                       </g>
                                     </g>
                                   </g>
                                 </svg>
                               </a>
                               <div class="panel">
                                 <p>
                                   Questo prodotto è stampato in 3d con il PLA, un materiale biodegradabile derivato da piante come il mais, il grano o la barbabietola
                                 </p>
                               </div>
                               <a href="" class="main_category">
<svg
  xmlns="http://www.w3.org/2000/svg"
  height="80"
  viewBox="0 0 512.00061 512"
  width="50"
>
  <g>
    <path
      d="m412 136c0 5.519531 4.480469 10 10 10s10-4.480469 10-10-4.480469-10-10-10-10 4.480469-10 10zm0 0"
      data-original="#000000"
      class="active-path"
      data-old_color="#000000"
      fill="#3B3B3B"
    />
    <path
      d="m452 256c0 12.808594-1.285156 25.59375-3.816406 38-1.105469 5.410156 2.386718 10.691406 7.796875 11.796875.675781.140625 1.347656.207031 2.011719.207031 4.652343 0 8.820312-3.269531 9.789062-8.003906 2.800781-13.722656 4.21875-27.851562 4.21875-42 0-31.488281-6.828125-61.789062-20.300781-90.0625-2.375-4.984375-8.339844-7.101562-13.328125-4.726562-4.988282 2.375-7.101563 8.34375-4.726563 13.332031 12.179688 25.5625 18.355469 52.96875 18.355469 81.457031zm0 0"
      data-original="#000000"
      class="active-path"
      data-old_color="#000000"
      fill="#3B3B3B"
    />
    <path
      d="m306 346h-10v-130c0-5.523438-4.476562-10-10-10h-80c-5.523438 0-10 4.476562-10 10v40c0 5.523438 4.476562 10 10 10h10v80h-10c-5.523438 0-10 4.476562-10 10v40c0 5.523438 4.476562 10 10 10h100c5.523438 0 10-4.476562 10-10v-40c0-5.523438-4.476562-10-10-10zm-10 40h-80v-20h10c5.523438 0 10-4.476562 10-10v-100c0-5.523438-4.476562-10-10-10h-10v-20h60v130c0 5.523438 4.480469 10 10 10h10zm0 0"
      data-original="#000000"
      class="active-path"
      data-old_color="#000000"
      fill="#3B3B3B"
    />
    <path
      d="m256 186c22.054688 0 40-17.945312 40-40s-17.945312-40-40-40-40 17.945312-40 40 17.945312 40 40 40zm0-60c11.027344 0 20 8.972656 20 20s-8.972656 20-20 20-20-8.972656-20-20 8.972656-20 20-20zm0 0"
      data-original="#000000"
      class="active-path"
      data-old_color="#000000"
      fill="#3B3B3B"
    />
    <path
      d="m256 0c-137.976562 0-256 117.800781-256 256 0 47.207031 13.527344 97.410156 36.335938 135.382812l-35.824219 107.457032c-1.195313 3.589844-.261719 7.554687 2.417969 10.230468 2.691406 2.691407 6.660156 3.609376 10.234374 2.414063l107.457032-35.820313c37.96875 22.8125 88.171875 36.335938 135.378906 36.335938 138.011719 0 256-117.816406 256-256 0-138.011719-117.8125-256-256-256zm0 492c-45.285156 0-93.417969-13.363281-128.757812-35.746094-2.503907-1.585937-5.625-2.003906-8.515626-1.039062l-92.914062 30.972656 30.972656-92.914062c.953125-2.851563.570313-5.976563-1.039062-8.515626-22.382813-35.335937-35.746094-83.472656-35.746094-128.757812 0-127.925781 108.074219-236 236-236s236 108.074219 236 236-108.074219 236-236 236zm0 0"
      data-original="#000000"
      class="active-path"
      data-old_color="#000000"
      fill="#3B3B3B"
    />
  </g>
</svg>
  <p>Maggiori Informazioni</p>
  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background: new 0 0 512 512;" xml:space="preserve" width="20px" height="20px">
    <g>
      <g>
        <g>
          <path d="M492,236H276V20c0-11.046-8.954-20-20-20c-11.046,0-20,8.954-20,20v216H20c-11.046,0-20,8.954-20,20s8.954,20,20,20h216    v216c0,11.046,8.954,20,20,20s20-8.954,20-20V276h216c11.046,0,20-8.954,20-20C512,244.954,503.046,236,492,236z" data-original="#3b3b3b" class="active-path scroll" data-old_color="#3b3b3b" fill="$grafiteColor"></path>
        </g>
      </g>
    </g>
  </svg>
</a>
<div class="panel">
  <p>
    ${element.info}
  </p>
</div>
                               <a href="" class="main_category">
                               <svg
  xmlns="http://www.w3.org/2000/svg"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  version="1.1"
  id="Capa_1"
  x="0px"
  y="0px"
  width="50"
  height="50"
  viewBox="0 0 441.732 441.732"
  style="enable-background: new 0 0 441.732 441.732;"
  xml:space="preserve"
>
  <g>
    <g>
      <g>
        <polygon
          points="342.289,32.594 314.181,50.799 311.618,65.33 339.73,47.125   "
          data-original="#000000"
          class="active-path"
          data-old_color="#000000"
          fill="#3B3B3B"
        />
        <polygon
          points="337.032,62.381 308.934,80.581 305.977,97.298 334.086,79.092   "
          data-original="#000000"
          class="active-path"
          data-old_color="#000000"
          fill="#3B3B3B"
        />
        <polygon
          points="347.29,4.257 323.147,0 316.876,35.541 344.976,17.34   "
          data-original="#000000"
          class="active-path"
          data-old_color="#000000"
          fill="#3B3B3B"
        />
        <path
          d="M267.251,155.936c-8.377,0-15.174,6.791-15.174,15.173c0,8.389,6.797,15.18,15.174,15.18c0.389,0,0.762-0.09,1.135-0.112    c3.661,26.969-24.635,51.625-51.435,53.078c-30.184,1.629-42.702-27.605-45.202-53.397c6.709-1.567,11.712-7.56,11.712-14.742    c0-8.378-6.793-15.174-15.174-15.174c-8.377,0-15.173,6.791-15.173,15.174c0,4.646,2.128,8.747,5.416,11.533    c2.22,35.485,22.442,74.765,64.052,69.129c34.83-4.717,65.965-38.285,57.522-72.662c1.457-2.332,2.325-5.054,2.325-8    C282.426,162.732,275.629,155.936,267.251,155.936z"
          data-original="#000000"
          class="active-path"
          data-old_color="#000000"
          fill="#3B3B3B"
        />
        <path
          d="M382.406,426.771c-11.159-43.923-44.178-194.693-0.588-290.595c0.046-0.093,0.052-0.194,0.093-0.29    c0.235-0.562,0.407-1.127,0.533-1.715c0.032-0.145,0.08-0.285,0.107-0.425c0.12-0.728,0.158-1.469,0.12-2.207    c-0.005-0.135-0.05-0.271-0.06-0.4c-0.065-0.593-0.174-1.182-0.355-1.765c-0.061-0.208-0.136-0.407-0.214-0.61    c-0.191-0.509-0.431-1.004-0.711-1.485c-0.064-0.118-0.098-0.257-0.182-0.386c-0.041-0.074-0.112-0.118-0.153-0.192    c-0.08-0.112-0.126-0.235-0.206-0.347l-31.349-42.148c-1.11-1.502-2.648-2.567-4.347-3.215c3.613,8.758,3.007,19.403-1.825,27.519    l10.356,13.929H309.9l18.676-12.099l2.825-15.986l-28.104,18.206l-1.751,9.88h-29.452l14.392-22.696l3.277-5.321l-22.199-14.131    l-53.426-33.877l-16.085,25.366l15.609,50.665h-8.073L172.888,16.29L87.207,42.687l11.575,37.588h-8.981    c-3.648,0-6.994,2.038-8.656,5.289l-21.613,42.148c-0.096,0.187-0.134,0.38-0.225,0.572c-0.112,0.246-0.208,0.493-0.294,0.755    c-0.159,0.466-0.277,0.936-0.356,1.415c-0.043,0.222-0.093,0.441-0.118,0.67c-0.06,0.635-0.071,1.262-0.011,1.888    c0.007,0.084-0.005,0.164,0,0.249c0.086,0.706,0.261,1.406,0.495,2.082c0.023,0.058,0.023,0.113,0.039,0.17    c46.047,125.29,13.634,255.43,2.599,292.333c-0.631,2.122-1.807,6.085,1.166,10.024c1.834,2.435,4.72,3.863,7.762,3.863h0.128    c2.638-0.033,4.566-0.939,6-2.209h291.263c1.385,1.021,3.192,1.732,5.565,1.732c3.006,0,5.841-1.388,7.685-3.759    C384.166,433.721,383.271,430.186,382.406,426.771z M95.752,99.732h9.028l6.99,22.695H84.102L95.752,99.732z M84.046,420.068    c12.849-48.63,35.138-163.794-2.197-278.18h276.61c-34.768,93.836-10.332,224.637,2.278,278.18H84.046z"
          data-original="#000000"
          class="active-path"
          data-old_color="#000000"
          fill="#3B3B3B"
        />
      </g>
    </g>
  </g>
</svg>
                                 <p>Cosa è incluso</p>
                                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background: new 0 0 512 512;" xml:space="preserve" width="20px" height="20px">
                                   <g>
                                     <g>
                                       <g>
                                         <path d="M492,236H276V20c0-11.046-8.954-20-20-20c-11.046,0-20,8.954-20,20v216H20c-11.046,0-20,8.954-20,20s8.954,20,20,20h216    v216c0,11.046,8.954,20,20,20s20-8.954,20-20V276h216c11.046,0,20-8.954,20-20C512,244.954,503.046,236,492,236z" data-original="#3b3b3b" class="active-path scroll" data-old_color="#3b3b3b" fill="$grafiteColor"></path>
                                       </g>
                                     </g>
                                   </g>
                                 </svg>
                               </a>
                               <div class="panel">
                                 <p>
                                   ${element.incluso}
                                 </p>
                               </div>
                               <a href="" class="main_category">
                               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background: new 0 0 512 512;" xml:space="preserve" width ="50" height="50" class="">
               <g>
                 <g>
                   <g>
                     <path d="M503.673,243.579c-5.323-6.344-12.584-10.467-20.908-12.008l-3.91-53.066c3.857-0.037,7.146-2.815,7.818-6.622    l1.508-8.551c1.616-9.166-0.638-18.214-6.185-24.823c-5.54-6.602-13.651-10.239-22.84-10.239h-58.854l2.865-16.245    c0.813-4.614-0.365-9.221-3.231-12.637c-2.838-3.382-7.105-5.322-11.707-5.322H98.524c-8.567,0-16.453,6.665-17.954,15.176    L54.939,254.609c-0.768,4.36,2.143,8.518,6.503,9.286c0.47,0.083,0.938,0.123,1.401,0.123c3.817,0,7.2-2.737,7.885-6.626    L96.36,112.025c0.169-0.957,1.401-1.927,2.163-1.927l288.702,0.001l-25.481,144.51c-0.769,4.36,2.142,8.518,6.503,9.286    c4.355,0.767,8.518-2.143,9.286-6.503l1.848-10.479h95.866c0.003,0,0.007,0,0.011,0s0.008,0,0.011,0    c6.626,0.001,12.351,2.476,16.122,6.969c2.595,3.093,4.109,6.943,4.473,11.202h-26.629c-3.891,0-7.219,2.793-7.895,6.625    l-3.015,17.102c-1.215,6.89,0.501,13.717,4.707,18.73c4.148,4.945,10.445,7.78,17.274,7.78h7.548l-6.22,35.273h-21.165    c-1.393-7.055-4.442-13.544-9.063-19.049c-8.199-9.773-20.265-15.154-33.972-15.154c-21.979,0-43.184,14.38-53.111,34.204h-3.223    l10.403-58.999c0.768-4.36-2.142-8.518-6.503-9.286c-4.358-0.77-8.518,2.142-9.286,6.503l-10.894,61.783H196.418    c-1.393-7.055-4.442-13.543-9.063-19.049c-8.2-9.773-20.265-15.154-33.973-15.154c-21.979,0-43.184,14.38-53.111,34.204    l-45.978-0.001l3.204-18.17h36.029c4.427,0,8.017-3.589,8.017-8.017c0-4.427-3.589-8.017-8.017-8.017H8.017    c-4.427,0-8.017,3.589-8.017,8.017c0,4.427,3.589,8.017,8.017,8.017h33.201l-2.865,16.245c-0.813,4.614,0.365,9.221,3.231,12.637    c2.838,3.382,7.105,5.322,11.707,5.322h41.774c-2.173,13.599,1.093,26.41,9.268,36.151c8.2,9.773,20.265,15.154,33.973,15.154    c27.284,0,53.387-22.151,58.188-49.38c0.113-0.645,0.202-1.286,0.292-1.926h162.331c-2.174,13.598,1.092,26.409,9.268,36.151    c8.2,9.773,20.265,15.154,33.973,15.154c27.284,0,53.387-22.151,58.188-49.38c0.113-0.645,0.202-1.286,0.292-1.926h27.525    c3.891,0,7.219-2.793,7.895-6.625l15.078-85.51C513.382,262.886,510.661,251.907,503.673,243.579z M382.21,230.883l9.235-52.375    h71.336l3.859,52.375H382.21z M472.391,160.549l-0.34,1.926h-77.78l3.204-18.171h61.681c4.367,0,8.117,1.602,10.557,4.511    C472.243,151.829,473.195,155.995,472.391,160.549z M180.705,365.773c-3.512,19.923-22.533,36.13-42.399,36.13    c-8.886,0-16.59-3.348-21.691-9.426c-5.248-6.255-7.248-14.749-5.631-23.919c3.513-19.923,22.533-36.13,42.399-36.13    c8.886,0,16.59,3.348,21.691,9.427C180.322,348.108,182.322,356.603,180.705,365.773z M444.756,365.773    c-3.513,19.923-22.533,36.13-42.399,36.13c-8.886,0-16.59-3.348-21.691-9.427c-5.248-6.255-7.248-14.749-5.631-23.919    c3.512-19.923,22.533-36.13,42.399-36.13c8.885,0,16.59,3.348,21.691,9.427C444.373,348.108,446.373,356.603,444.756,365.773z     M490.681,299.292h-10.375v-0.001c-2.139,0-3.865-0.71-4.992-2.052c-1.169-1.394-1.596-3.397-1.2-5.64l1.848-10.477h17.923    L490.681,299.292z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3b3b3b"></path>
                   </g>
                 </g>
                 <g>
                   <g>
                     <path d="M159.06,355.919c-2.838-3.382-7.105-5.322-11.708-5.322c-8.567,0-16.453,6.665-17.954,15.176    c-0.813,4.614,0.365,9.221,3.231,12.637c2.838,3.382,7.105,5.322,11.707,5.322c8.567,0,16.453-6.666,17.954-15.175    C163.104,363.942,161.927,359.336,159.06,355.919z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3b3b3b"></path>
                   </g>
                 </g>
                 <g>
                   <g>
                     <path d="M423.111,355.919c-2.839-3.382-7.106-5.322-11.707-5.322c-8.567,0-16.453,6.665-17.953,15.175    c-0.813,4.615,0.363,9.221,3.23,12.638c2.838,3.382,7.105,5.322,11.707,5.322c8.567,0,16.453-6.666,17.954-15.175    C427.156,363.942,425.978,359.336,423.111,355.919z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3b3b3b"></path>
                   </g>
                 </g>
                 <g>
                   <g>
                     <path d="M323.374,316.393H221.791c-4.427,0-8.017,3.589-8.017,8.017c0,4.427,3.589,8.017,8.017,8.017h101.583    c4.427,0,8.017-3.589,8.017-8.017C331.39,319.982,327.801,316.393,323.374,316.393z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3b3b3b"></path>
                   </g>
                 </g>
                 <g>
                   <g>
                     <path d="M179.036,282.189H31.15c-4.427,0-8.017,3.589-8.017,8.017c0,4.427,3.588,8.017,8.017,8.017h147.886    c4.427,0,8.017-3.589,8.017-8.017C187.053,285.778,183.464,282.189,179.036,282.189z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3b3b3b"></path>
                   </g>
                 </g>
                 <g>
                   <g>
                     <path d="M332.127,249.936l-29.68-25.653c-3.35-2.894-8.412-2.527-11.308,0.823c-2.896,3.35-2.527,8.412,0.823,11.308    l13.388,11.572H102.077c-4.427,0-8.017,3.589-8.017,8.017c0,4.427,3.589,8.017,8.017,8.017h198.189l-16.535,10.954    c-3.692,2.444-4.701,7.419-2.256,11.11c1.542,2.329,4.092,3.59,6.69,3.59c1.52,0,3.058-0.432,4.42-1.335l38.727-25.653    c2.092-1.384,3.413-3.668,3.573-6.172C335.045,254.009,334.025,251.575,332.127,249.936z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3b3b3b"></path>
                   </g>
                 </g>
               </g>
             </svg>
                                 <p>Costi di spedizione</p>
                                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background: new 0 0 512 512;" xml:space="preserve" width="20px" height="20px">
                                   <g>
                                     <g>
                                       <g>
                                         <path d="M492,236H276V20c0-11.046-8.954-20-20-20c-11.046,0-20,8.954-20,20v216H20c-11.046,0-20,8.954-20,20s8.954,20,20,20h216    v216c0,11.046,8.954,20,20,20s20-8.954,20-20V276h216c11.046,0,20-8.954,20-20C512,244.954,503.046,236,492,236z" data-original="#3b3b3b" class="active-path scroll" data-old_color="#3b3b3b" fill="$grafiteColor"></path>
                                       </g>
                                     </g>
                                   </g>
                                 </svg>
                               </a>
                               <div class="panel">
                                 <p>
                                     ${element.spedizione}
                                 </p>
                               </div>
                               <a href="" class="main_category">
                               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512.001 512.001" style="enable-background: new 0 0 512.001 512.001;" xml:space="preserve" width  ="50" height="50">
                               <g>
                                 <g>
                                   <g>
                                     <path d="M407.04,385.22c-4.539-3.148-10.77-2.016-13.915,2.523c-3.048,4.4-6.502,8.678-10.271,12.717    c-2.525,2.709-5.999,5.975-9.779,9.191c-4.206,3.58-4.714,9.891-1.135,14.098c1.978,2.324,4.791,3.518,7.621,3.518    c2.291,0,4.594-0.783,6.477-2.385c4.41-3.754,8.369-7.482,11.443-10.783c4.422-4.738,8.488-9.773,12.084-14.965    C412.71,394.595,411.58,388.365,407.04,385.22z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3b3b3b"></path>
                                   </g>
                                 </g>
                                 <g>
                                   <g>
                                     <path d="M363.474,430.058c-2.346-5-8.298-7.152-13.3-4.807l-0.313,0.141c-5.053,2.229-7.341,8.133-5.11,13.186    c1.65,3.74,5.312,5.965,9.153,5.965c1.349,0,2.72-0.275,4.032-0.854c0.244-0.107,0.486-0.217,0.729-0.33    C363.665,441.013,365.818,435.06,363.474,430.058z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3b3b3b"></path>
                                   </g>
                                 </g>
                                 <g>
                                   <g>
                                     <path d="M482.126,26.001H29.875C13.401,26.001,0,39.404,0,55.876v280.252c0,16.471,13.401,29.873,29.875,29.873h210.586    c8.927,37.77,29.114,64.52,46.757,81.658C312.97,472.677,342.49,486,353.899,486c11.408,0,40.928-13.322,66.681-38.34    c17.643-17.139,37.831-43.889,46.757-81.658h14.789c16.473,0,29.875-13.402,29.875-29.873V55.876    C512.001,39.404,498.599,26.001,482.126,26.001z M452,325.019c0.001,52.688-24.32,87.637-44.724,107.678    c-24.203,23.773-49.03,33.02-53.377,33.301c-4.348-0.281-29.176-9.527-53.379-33.301c-20.402-20.041-44.723-54.99-44.723-107.678    v-49.246c34.43-9.957,66.646-23.584,98.105-41.492c30.75,17.453,64.41,31.686,98.098,41.486V325.019z M492.001,336.128    c0,5.444-4.431,9.873-9.875,9.873h-11.311c0.766-6.701,1.186-13.689,1.186-20.982v-56.828c0-4.514-3.024-8.467-7.379-9.65    c-36.313-9.861-72.854-25.227-105.672-44.436c-3.12-1.826-6.982-1.824-10.104,0.002c-33.748,19.754-68.313,34.287-105.67,44.434    c-4.355,1.184-7.379,5.137-7.379,9.65v56.828c0,7.293,0.419,14.281,1.186,20.982H29.875c-5.445,0-9.875-4.43-9.875-9.873V165.999    h472.001V336.128z M492.001,146H20v-39.998h472.001V146z M492.001,86.001H20V55.876c0-5.445,4.43-9.875,9.875-9.875h452.251    c5.444,0,9.875,4.43,9.875,9.875V86.001z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3b3b3b"></path>
                                   </g>
                                 </g>
                                 <g>
                                   <g>
                                     <path d="M119.997,236.003c-6.029,0-11.982,1.219-17.492,3.543c-5.383-2.281-11.299-3.543-17.503-3.543    c-24.813,0-45.001,20.186-45.001,44.998s20.187,45,45.001,45c6.204,0,12.12-1.262,17.502-3.543    c5.511,2.324,11.464,3.543,17.493,3.543c24.814,0,45.002-20.188,45.002-45S144.811,236.003,119.997,236.003z M85.002,306.001    c-13.786,0-25.001-11.214-25.001-25c0-13.785,11.215-24.998,25.001-24.998c13.784,0,24.999,11.213,24.999,24.998    C110.001,294.786,98.786,306.001,85.002,306.001z M122.476,305.879c4.75-7.131,7.525-15.686,7.525-24.877    c0-9.191-2.775-17.744-7.524-24.875c12.625,1.248,22.521,11.928,22.521,24.875C144.998,293.949,135.102,304.63,122.476,305.879z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3b3b3b"></path>
                                   </g>
                                 </g>
                                 <g>
                                   <g>
                                     <path d="M101.436,191.001H49.999c-5.523,0-10,4.477-10,10c0,5.522,4.477,10,10,10h51.437c5.523,0,10-4.479,10-10    C111.436,195.478,106.959,191.001,101.436,191.001z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3b3b3b"></path>
                                   </g>
                                 </g>
                                 <g>
                                   <g>
                                     <path d="M130.996,191.001h-0.474c-5.523,0-10,4.477-10,10c0,5.522,4.477,10,10,10h0.474c5.522,0,10-4.479,10-10    C140.996,195.478,136.518,191.001,130.996,191.001z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3b3b3b"></path>
                                   </g>
                                 </g>
                                 <g>
                                   <g>
                                     <path d="M408.79,302.026c-3.903-3.903-10.234-3.905-14.141-0.001l-53.477,53.473l-28.023-28.025    c-3.906-3.902-10.238-3.904-14.143,0c-3.905,3.906-3.906,10.238,0,14.143l35.095,35.096c1.953,1.953,4.512,2.93,7.071,2.93    s5.119-0.977,7.07-2.93l60.547-60.543C412.695,312.265,412.695,305.933,408.79,302.026z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3b3b3b"></path>
                                   </g>
                                 </g>
                               </g>
                             </svg>
                                 <p>Pagamenti</p>
                                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background: new 0 0 512 512;" xml:space="preserve" width="20px" height="20px">
                                   <g>
                                     <g>
                                       <g>
                                         <path d="M492,236H276V20c0-11.046-8.954-20-20-20c-11.046,0-20,8.954-20,20v216H20c-11.046,0-20,8.954-20,20s8.954,20,20,20h216    v216c0,11.046,8.954,20,20,20s20-8.954,20-20V276h216c11.046,0,20-8.954,20-20C512,244.954,503.046,236,492,236z" data-original="#3b3b3b" class="active-path scroll" data-old_color="#3b3b3b" fill="$grafiteColor"></path>
                                       </g>
                                     </g>
                                   </g>
                                 </svg>
                               </a>
                               <div class="panel">
                                 <p>
                                     Pagamenti sicuri e protetti con Paypal. Non è necessario creare un account, è possibile pagare direttamente con carte di credito Visa, Mastercard e American Express
                                 </p>
                               </div>
                               <a href="" class="main_category">
                               <svg xmlns="http://www.w3.org/2000/svg" height="50" version="1.1" viewBox="-42 0 511 511.99956" width  ="50">
               <g>
                 <g id="surface1">
                   <path d="M 423.15625 277.039062 L 376.34375 251.3125 C 373.082031 249.519531 369.105469 249.648438 365.96875 251.648438 L 317.027344 282.792969 L 252.609375 282.792969 C 243.4375 280.21875 232.898438 278.257812 221.265625 277.722656 C 221.042969 260.730469 222.285156 215.085938 239.613281 173.929688 C 245.5625 176.265625 254.683594 178.894531 266.050781 179.15625 C 266.679688 179.171875 267.308594 179.179688 267.933594 179.179688 C 300.460938 179.179688 326.625 159.71875 342.957031 143.210938 C 392.226562 93.425781 409.511719 14.101562 410.222656 10.75 C 410.226562 10.726562 410.230469 10.703125 410.234375 10.679688 C 410.269531 10.511719 410.292969 10.339844 410.316406 10.167969 C 410.324219 10.09375 410.339844 10.019531 410.347656 9.945312 C 410.367188 9.761719 410.371094 9.574219 410.378906 9.390625 C 410.378906 9.324219 410.386719 9.261719 410.386719 9.195312 C 410.386719 9.015625 410.375 8.832031 410.359375 8.648438 C 410.355469 8.582031 410.355469 8.511719 410.347656 8.441406 C 410.332031 8.28125 410.304688 8.128906 410.277344 7.96875 C 410.261719 7.875 410.25 7.777344 410.230469 7.683594 C 410.207031 7.558594 410.171875 7.4375 410.136719 7.3125 C 410.105469 7.1875 410.078125 7.0625 410.039062 6.9375 C 410.03125 6.925781 410.03125 6.910156 410.027344 6.894531 C 409.992188 6.789062 409.949219 6.691406 409.910156 6.589844 C 409.867188 6.46875 409.824219 6.347656 409.773438 6.230469 C 409.710938 6.082031 409.640625 5.941406 409.566406 5.800781 C 409.527344 5.71875 409.492188 5.636719 409.445312 5.558594 C 409.347656 5.378906 409.234375 5.203125 409.121094 5.03125 C 409.097656 5 409.082031 4.964844 409.058594 4.933594 C 409.058594 4.929688 409.054688 4.925781 409.050781 4.921875 C 408.617188 4.292969 408.09375 3.742188 407.503906 3.285156 C 407.441406 3.234375 407.378906 3.195312 407.316406 3.148438 C 407.179688 3.050781 407.046875 2.953125 406.90625 2.863281 C 406.808594 2.800781 406.707031 2.75 406.609375 2.691406 C 406.496094 2.628906 406.382812 2.558594 406.265625 2.5 C 406.15625 2.445312 406.042969 2.398438 405.933594 2.347656 C 405.820312 2.300781 405.707031 2.246094 405.59375 2.203125 C 405.480469 2.160156 405.363281 2.121094 405.25 2.085938 C 405.128906 2.042969 405.007812 2.003906 404.882812 1.96875 C 404.773438 1.9375 404.660156 1.914062 404.546875 1.886719 C 404.414062 1.859375 404.28125 1.828125 404.148438 1.804688 C 404.109375 1.796875 404.070312 1.785156 404.035156 1.78125 C 402.457031 1.535156 364.9375 -4.074219 324.398438 5.972656 C 291.226562 14.195312 248.816406 36.726562 229.28125 75.292969 C 215.816406 101.875 215.296875 131.59375 227.722656 163.667969 C 220.925781 178.71875 216.238281 194.304688 213.023438 209.101562 C 209 195.9375 203.253906 181.445312 195.113281 168.164062 C 200.566406 144.667969 197.304688 123.949219 185.363281 106.554688 C 167.921875 81.152344 135.941406 69.25 111.832031 66.585938 C 82.53125 63.34375 56.777344 70.785156 55.695312 71.105469 C 55.660156 71.117188 55.625 71.132812 55.585938 71.144531 C 55.457031 71.183594 55.332031 71.230469 55.207031 71.277344 C 55.097656 71.320312 54.984375 71.359375 54.878906 71.402344 C 54.761719 71.453125 54.652344 71.507812 54.539062 71.5625 C 54.425781 71.617188 54.3125 71.671875 54.207031 71.730469 C 54.101562 71.789062 54.003906 71.851562 53.902344 71.914062 C 53.792969 71.980469 53.683594 72.042969 53.578125 72.117188 C 53.476562 72.183594 53.378906 72.261719 53.28125 72.335938 C 53.183594 72.410156 53.085938 72.480469 52.992188 72.558594 C 52.871094 72.660156 52.757812 72.773438 52.640625 72.882812 C 52.582031 72.941406 52.515625 72.996094 52.457031 73.054688 C 51.929688 73.589844 51.488281 74.207031 51.136719 74.886719 C 51.136719 74.890625 51.132812 74.890625 51.132812 74.894531 C 51.117188 74.925781 51.105469 74.957031 51.089844 74.988281 C 50.996094 75.179688 50.910156 75.371094 50.832031 75.570312 C 50.796875 75.652344 50.773438 75.734375 50.746094 75.816406 C 50.691406 75.96875 50.636719 76.121094 50.59375 76.277344 C 50.558594 76.398438 50.535156 76.519531 50.507812 76.644531 C 50.484375 76.753906 50.453125 76.859375 50.433594 76.96875 C 50.429688 76.984375 50.429688 77.003906 50.425781 77.019531 C 50.40625 77.140625 50.394531 77.265625 50.378906 77.386719 C 50.363281 77.519531 50.34375 77.652344 50.332031 77.78125 C 50.328125 77.875 50.328125 77.964844 50.324219 78.054688 C 50.320312 78.21875 50.3125 78.386719 50.320312 78.550781 C 50.320312 78.613281 50.328125 78.675781 50.332031 78.738281 C 50.34375 78.929688 50.355469 79.117188 50.382812 79.304688 C 50.390625 79.359375 50.402344 79.417969 50.410156 79.476562 C 50.441406 79.667969 50.472656 79.855469 50.519531 80.039062 C 50.535156 80.105469 50.554688 80.171875 50.574219 80.238281 C 50.621094 80.414062 50.671875 80.585938 50.726562 80.753906 C 50.734375 80.777344 50.738281 80.796875 50.746094 80.816406 C 51.554688 83.085938 70.984375 136.746094 110.027344 166.910156 C 121.640625 175.882812 138.945312 185.757812 158.847656 185.757812 C 161.863281 185.757812 164.941406 185.53125 168.0625 185.039062 C 175 183.949219 180.554688 181.8125 184.5625 179.808594 C 201.695312 210.148438 206.273438 246.988281 206.914062 252.882812 C 206.207031 263.390625 206.140625 272.035156 206.234375 277.804688 C 195.171875 278.425781 185.121094 280.324219 176.328125 282.792969 L 111.910156 282.792969 L 62.972656 251.648438 C 59.832031 249.652344 55.855469 249.523438 52.59375 251.3125 L 5.78125 277.039062 C 2.617188 278.777344 0.648438 281.96875 0.507812 285.574219 C 0.367188 289.179688 2.089844 292.515625 5.109375 294.492188 L 67.457031 335.289062 L 67.457031 359.222656 C 67.457031 363.363281 70.816406 366.722656 74.957031 366.722656 C 79.097656 366.722656 82.457031 363.363281 82.457031 359.222656 L 82.457031 338.730469 L 174.972656 338.730469 L 174.972656 384.449219 C 174.972656 387.828125 176.636719 390.988281 179.417969 392.90625 C 182.207031 394.820312 185.753906 395.238281 188.910156 394.027344 L 213.722656 384.5 L 238.539062 394.027344 C 239.726562 394.484375 240.976562 394.710938 242.214844 394.710938 C 244.261719 394.710938 246.292969 394.097656 248.027344 392.90625 C 250.8125 390.992188 252.476562 387.828125 252.476562 384.449219 L 252.476562 338.730469 L 346.480469 338.730469 L 346.480469 488.011719 C 346.480469 492.96875 342.445312 497 337.492188 497 L 91.445312 497 C 86.488281 497 82.457031 492.96875 82.457031 488.011719 L 82.457031 394.121094 C 82.457031 389.980469 79.097656 386.621094 74.957031 386.621094 C 70.816406 386.621094 67.457031 389.980469 67.457031 394.121094 L 67.457031 488.011719 C 67.457031 501.238281 78.21875 512 91.445312 512 L 337.492188 512 C 350.71875 512 361.480469 501.238281 361.480469 488.011719 L 361.480469 335.289062 L 423.828125 294.492188 C 426.847656 292.515625 428.570312 289.183594 428.429688 285.574219 C 428.289062 281.96875 426.320312 278.777344 423.15625 277.039062 Z M 242.660156 82.070312 C 259.925781 47.984375 299.425781 27.617188 328.007812 20.53125 C 341.957031 17.074219 355.675781 15.640625 367.429688 15.203125 C 334.046875 31.089844 304.464844 53.757812 279.191406 82.886719 C 276.476562 86.015625 276.8125 90.753906 279.941406 93.46875 C 281.359375 94.699219 283.109375 95.300781 284.851562 95.300781 C 286.949219 95.300781 289.035156 94.425781 290.519531 92.71875 C 318.917969 59.984375 353.035156 35.890625 392.078125 20.949219 C 385.078125 45.292969 366.894531 97.699219 332.296875 132.664062 C 293.226562 172.144531 259.363281 165.46875 245.933594 160.359375 C 253.105469 145.957031 261.09375 132.300781 269.734375 119.683594 C 272.074219 116.265625 271.203125 111.601562 267.785156 109.257812 C 264.371094 106.917969 259.703125 107.789062 257.359375 111.207031 C 250.1875 121.675781 243.460938 132.835938 237.246094 144.503906 C 231.3125 121.867188 233.113281 100.921875 242.660156 82.070312 Z M 119.199219 155.039062 C 94.746094 136.148438 78.304688 105.941406 70.371094 88.792969 C 92.925781 94.789062 113.820312 105.21875 132.636719 119.933594 C 148.84375 132.605469 163.550781 148.449219 176.441406 167.082031 C 166.878906 171.375 146.246094 175.933594 119.199219 155.039062 Z M 182.578125 149.992188 C 162.996094 124.097656 134.253906 96.542969 94.09375 80.804688 C 99.210938 80.707031 104.636719 80.882812 110.183594 81.492188 C 130.039062 83.691406 158.402344 93.792969 172.996094 115.046875 C 179.9375 125.15625 183.128906 136.867188 182.578125 149.992188 Z M 374.019531 264.300781 C 374.023438 264.300781 374.023438 264.300781 374.023438 264.300781 C 374.019531 264.300781 374.015625 264.304688 374.011719 264.304688 Z M 54.917969 264.300781 L 54.921875 264.304688 C 54.921875 264.304688 54.917969 264.300781 54.914062 264.300781 C 54.914062 264.300781 54.914062 264.300781 54.917969 264.300781 Z M 73.523438 321.332031 L 20.023438 286.324219 L 57.320312 265.832031 L 98.4375 291.996094 Z M 115.636719 323.734375 L 91.164062 323.734375 L 113.199219 297.792969 L 141.660156 297.792969 C 128.3125 306 119.160156 315.191406 115.636719 323.734375 Z M 237.476562 377.554688 L 217.40625 369.847656 C 215.03125 368.933594 212.417969 368.933594 210.046875 369.84375 L 189.96875 377.554688 L 189.96875 338.730469 L 237.476562 338.730469 Z M 244.976562 323.734375 L 133.285156 323.734375 C 136.847656 319.589844 142.898438 314.3125 152.335938 308.890625 C 158.101562 305.574219 167.277344 301.042969 179.101562 297.59375 C 179.367188 297.53125 179.625 297.457031 179.878906 297.367188 C 189.621094 294.59375 201.117188 292.578125 213.960938 292.578125 L 214.972656 292.578125 C 227.820312 292.578125 239.3125 294.59375 249.054688 297.367188 C 249.308594 297.457031 249.570312 297.53125 249.835938 297.59375 C 261.65625 301.042969 270.835938 305.574219 276.601562 308.890625 C 286.039062 314.3125 292.085938 319.589844 295.652344 323.734375 Z M 313.300781 323.734375 C 309.777344 315.191406 300.625 306 287.273438 297.792969 L 315.738281 297.792969 L 337.769531 323.734375 Z M 355.414062 321.332031 L 330.496094 291.996094 L 371.617188 265.832031 L 408.910156 286.324219 Z M 355.414062 321.332031 " data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3b3b3b"></path>
                 </g>
               </g>
             </svg>
                                 <p>Packaging</p>
                                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background: new 0 0 512 512;" xml:space="preserve" width="20px" height="20px">
                                   <g>
                                     <g>
                                       <g>
                                         <path d="M492,236H276V20c0-11.046-8.954-20-20-20c-11.046,0-20,8.954-20,20v216H20c-11.046,0-20,8.954-20,20s8.954,20,20,20h216    v216c0,11.046,8.954,20,20,20s20-8.954,20-20V276h216c11.046,0,20-8.954,20-20C512,244.954,503.046,236,492,236z" data-original="#3b3b3b" class="active-path scroll" data-old_color="#3b3b3b" fill="$grafiteColor"></path>
                                       </g>
                                     </g>
                                   </g>
                                 </svg>
                               </a>
                               <div class="panel">
                                 <p>
                                     I nostri packaging sono in cartone riciclato, progettati per spedizioni sia con piante che senza piante, utilizzando imballaggi e protezioni differenti per ogni singolo prodotto. Sempre inclusi nei costi di spedizione
                                 </p>
                               </div>
                               ${
                                 element['affitto']
                                   ? `<a href="" class="main_category">
                                   <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512.001 512.001" style="enable-background: new 0 0 512.001 512.001;" xml:space="preserve" width  ="50" height="50">
                                   <g>
                                     <g>
                                       <g>
                                         <path d="M477.922,174.683H320.166L289.351,83.38c-0.862-17.65-15.489-31.744-33.35-31.744c-17.86,0-32.488,14.094-33.35,31.743    l-30.817,91.302H34.079C15.288,174.683,0,189.97,0,208.762v217.524c0,18.792,15.288,34.079,34.079,34.079h36.413    c5.124,0,9.276-4.153,9.276-9.276s-4.152-9.276-9.276-9.276H34.079c-8.562,0-15.528-6.966-15.528-15.528V208.762    c0-8.562,6.966-15.528,15.528-15.528h164.385c0.01,0,0.02,0.003,0.03,0.003s0.021-0.003,0.031-0.003h114.95    c0.01,0,0.021,0.003,0.031,0.003s0.02-0.003,0.03-0.003h164.386c8.562,0,15.528,6.966,15.528,15.528v217.524    c0,8.562-6.966,15.528-15.528,15.528H100.175c-5.124,0-9.276,4.153-9.276,9.276s4.152,9.276,9.276,9.276h377.747    c18.79,0,34.079-15.288,34.079-34.079V208.762C512,189.97,496.712,174.683,477.922,174.683z M256,70.189    c8.182,0,14.84,6.658,14.84,14.84s-6.658,14.84-14.84,14.84s-14.84-6.658-14.84-14.84S247.818,70.189,256,70.189z     M211.413,174.683l21.977-65.109c5.953,5.488,13.895,8.847,22.609,8.847s16.658-3.359,22.609-8.847l21.975,65.109H211.413z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3B3B3B"></path>
                                       </g>
                                     </g>
                                     <g>
                                       <g>
                                         <path d="M323.054,263.051c-5.124,0-9.276,4.153-9.276,9.276v58.9L272.543,267.3c-2.231-3.457-6.472-5.032-10.416-3.87    c-3.945,1.161-6.655,4.784-6.655,8.897v90.392c0,5.123,4.152,9.276,9.276,9.276s9.276-4.153,9.276-9.276v-58.899l41.235,63.926    c1.74,2.697,4.703,4.248,7.795,4.248c0.872,0,1.754-0.124,2.621-0.378c3.945-1.161,6.655-4.784,6.655-8.897v-90.392    C332.33,267.205,328.178,263.051,323.054,263.051z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3B3B3B"></path>
                                       </g>
                                     </g>
                                     <g>
                                       <g>
                                         <path d="M116.94,326.22c11.333-5.266,19.212-16.75,19.212-30.047c0-18.262-14.857-33.12-33.118-33.12H83.326    c-5.124,0-9.276,4.153-9.276,9.276v90.392c0,5.123,4.152,9.276,9.276,9.276s9.276-4.153,9.276-9.276v-33.429h4.905l20.486,37.844    c1.678,3.102,4.87,4.862,8.166,4.862c1.49,0,3.004-0.361,4.407-1.12c4.506-2.439,6.18-8.068,3.741-12.573L116.94,326.22z     M103.058,310.738c-0.021,0-0.041,0.001-0.062,0.001H92.602c0,0,0-29.136,0-29.136h10.432c8.032,0,14.567,6.535,14.567,14.568    C117.601,304.195,111.078,310.724,103.058,310.738z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3B3B3B"></path>
                                       </g>
                                     </g>
                                     <g>
                                       <g>
                                         <path d="M220.3,327.041c5.124,0,9.276-4.153,9.276-9.276c0-5.123-4.152-9.276-9.276-9.276h-33.737v-26.885H220.3v-0.001    c5.124,0,9.276-4.153,9.276-9.276c0-5.123-4.152-9.276-9.276-9.276h-43.013c-5.124,0-9.276,4.153-9.276,9.276v90.392    c0,5.123,4.152,9.276,9.276,9.276H220.3c5.124,0,9.276-4.153,9.276-9.276c0-5.123-4.152-9.276-9.276-9.276h-33.737v-26.403H220.3z    " data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3B3B3B"></path>
                                       </g>
                                     </g>
                                     <g>
                                       <g>
                                         <path d="M395.22,337.319c-5.124,0-9.276,4.153-9.276,9.276v16.126c0,5.123,4.152,9.276,9.276,9.276s9.276-4.153,9.276-9.276    v-16.126C404.496,341.472,400.343,337.319,395.22,337.319z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3B3B3B"></path>
                                       </g>
                                     </g>
                                     <g>
                                       <g>
                                         <path d="M428.674,263.051h-66.908c-5.124,0-9.276,4.153-9.276,9.276c0,5.123,4.152,9.276,9.276,9.276h24.179v40.255    c0,5.123,4.152,9.276,9.276,9.276s9.276-4.153,9.276-9.276v-40.255h24.178c5.124,0,9.276-4.153,9.276-9.276    C437.949,267.205,433.797,263.051,428.674,263.051z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3B3B3B"></path>
                                       </g>
                                     </g>
                                   </g>
                                 </svg>
                                   <p>Noleggio</p>
                                   <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background: new 0 0 512 512;" xml:space="preserve" width="20px" height="20px">
                                     <g>
                                       <g>
                                         <g>
                                           <path d="M492,236H276V20c0-11.046-8.954-20-20-20c-11.046,0-20,8.954-20,20v216H20c-11.046,0-20,8.954-20,20s8.954,20,20,20h216    v216c0,11.046,8.954,20,20,20s20-8.954,20-20V276h216c11.046,0,20-8.954,20-20C512,244.954,503.046,236,492,236z" data-original="#3b3b3b" class="active-path scroll" data-old_color="#3b3b3b" fill="$grafiteColor"></path>
                                         </g>
                                       </g>
                                     </g>
                                   </svg>
                                 </a>
                                 <div class="panel">
                                   <p>
                                       Questo prodotto è disponibile anche per il servizio di noleggio. Per maggiori informazioni <a href="https://neasy-jungle-90f4c.firebaseapp.com/src/assets/html/noleggio.html">clicca qui</a>
                                   </p>
                                 </div>`
                                   : ''
                               }
                             </div>
                           </section>`;
          dettaglioInfo.innerHTML += htmlDetailInfo;
        } else {
          const htmlDetailInfo = `
        <section class="nj_homepage_section5" style="margin: 30px 15px">
                             <div class="accordion">
                               <a href="" class="main_category">
                               <svg
                               xmlns="http://www.w3.org/2000/svg"
                               height="50"
                               version="1.1"
                               viewBox="0 -51 512 512"
                               width="50"
                             >
                               <g>
                                 <g id="surface1">
                                   <path
                                     d="M 0 0 L 0 410 L 512 410 L 512 0 Z M 482 380 L 30 380 L 30 30 L 482 30 Z M 482 380 "
                                     data-original="#000000"
                                     class="active-path"
                                     data-old_color="#000000"
                                     fill="#3B3B3B"
                                   />
                                   <path
                                     d="M 180.375 310 L 121.214844 310 L 214.105469 217.105469 L 192.894531 195.894531 L 100 288.785156 L 100 229.625 L 70 229.625 L 70 340 L 180.375 340 Z M 180.375 310 "
                                     data-original="#000000"
                                     class="active-path"
                                     data-old_color="#000000"
                                     fill="#3B3B3B"
                                   />
                                   <path
                                     d="M 319.105469 214.105469 L 412 121.214844 L 412 180.375 L 442 180.375 L 442 70 L 331.625 70 L 331.625 100 L 390.785156 100 L 297.894531 192.894531 Z M 319.105469 214.105469 "
                                     data-original="#000000"
                                     class="active-path"
                                     data-old_color="#000000"
                                     fill="#3B3B3B"
                                   />
                                 </g>
                               </g>
                             </svg>                            
                                 <p>Dimensioni</p>
                                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background: new 0 0 512 512;" xml:space="preserve" width="20px" height="20px">
                                   <g>
                                     <g>
                                       <g>
                                         <path d="M492,236H276V20c0-11.046-8.954-20-20-20c-11.046,0-20,8.954-20,20v216H20c-11.046,0-20,8.954-20,20s8.954,20,20,20h216    v216c0,11.046,8.954,20,20,20s20-8.954,20-20V276h216c11.046,0,20-8.954,20-20C512,244.954,503.046,236,492,236z" data-original="#3b3b3b" class="active-path scroll" data-old_color="#3b3b3b" fill="$grafiteColor"></path>
                                       </g>
                                     </g>
                                   </g>
                                 </svg>
                               </a>
                               <div class="panel">
                                 <p>
                                   ${element.dimensioni}
                                 </p>
                               </div>
                               <a href="" class="main_category">
                               <svg
                               xmlns="http://www.w3.org/2000/svg"
                               viewBox="0 -40 512 511"
                               width="50"
                               height="50"
                             >
                               <g>
                                 <path
                                   d="m133.351562 20.5c2.628907 0 5.207032-1.070312 7.070313-2.929688 1.859375-1.859374 2.929687-4.441406 2.929687-7.070312s-1.070312-5.210938-2.929687-7.070312c-1.863281-1.859376-4.441406-2.929688-7.070313-2.929688-2.632812 0-5.210937 1.070312-7.070312 2.929688-1.859375 1.859374-2.929688 4.441406-2.929688 7.070312s1.070313 5.210938 2.929688 7.070312c1.859375 1.859376 4.4375 2.929688 7.070312 2.929688zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m337.75 412.4375h-279.75c-20.953125 0-38-17.046875-38-38v-315.9375c0-20.953125 17.046875-38 38-38h31c5.523438 0 10-4.476562 10-10s-4.476562-10-10-10h-31c-31.980469 0-58 26.019531-58 58v315.9375c0 31.980469 26.019531 58 58 58h279.75c5.523438 0 10-4.476562 10-10s-4.476562-10-10-10zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m377.941406 412.441406c-2.640625 0-5.210937 1.070313-7.070312 2.929688-1.871094 1.859375-2.929688 4.4375-2.929688 7.070312 0 2.628906 1.058594 5.207032 2.929688 7.070313 1.859375 1.859375 4.429687 2.929687 7.070312 2.929687 2.628906 0 5.207032-1.070312 7.070313-2.929687 1.859375-1.871094 2.929687-4.441407 2.929687-7.070313 0-2.632812-1.070312-5.210937-2.929687-7.070312-1.863281-1.859375-4.441407-2.929688-7.070313-2.929688zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m454 .5h-280.5c-5.523438 0-10 4.476562-10 10s4.476562 10 10 10h280.5c20.953125 0 38 17.046875 38 38v315.9375c0 20.953125-17.046875 38-38 38h-37.667969c-5.523437 0-10 4.476562-10 10s4.476563 10 10 10h37.667969c31.980469 0 58-26.019531 58-58v-315.9375c0-31.980469-26.019531-58-58-58zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m276.0625 68.5h12c5.523438 0 10-4.476562 10-10s-4.476562-10-10-10h-12c-5.523438 0-10 4.476562-10 10s4.476562 10 10 10zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m226.0625 68.5c5.523438 0 10-4.476562 10-10s-4.476562-10-10-10h-12c-5.523438 0-10 4.476562-10 10s4.476562 10 10 10zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m132 364.4375c-5.523438 0-10 4.476562-10 10s4.476562 10 10 10h12c5.523438 0 10-4.476562 10-10s-4.476562-10-10-10zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m48 150.4375c0 5.523438 4.476562 10 10 10s10-4.476562 10-10v-12c0-5.523438-4.476562-10-10-10s-10 4.476562-10 10zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m80.0625 58.5c0 5.523438 4.476562 10 10 10h12c5.523438 0 10-4.476562 10-10s-4.476562-10-10-10h-12c-5.523438 0-10 4.476562-10 10zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m58 98.4375c5.523438 0 10-4.476562 10-10v-12c0-5.523438-4.476562-10-10-10s-10 4.476562-10 10v12c0 5.523438 4.476562 10 10 10zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m48 274.4375c0 5.523438 4.476562 10 10 10s10-4.476562 10-10v-12c0-5.523438-4.476562-10-10-10s-10 4.476562-10 10zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m48 336.4375c0 5.523438 4.476562 10 10 10s10-4.476562 10-10v-12c0-5.523438-4.476562-10-10-10s-10 4.476562-10 10zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m48 212.4375c0 5.523438 4.476562 10 10 10s10-4.476562 10-10v-12c0-5.523438-4.476562-10-10-10s-10 4.476562-10 10zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m70 384.4375h12c5.523438 0 10-4.476562 10-10s-4.476562-10-10-10h-12c-5.523438 0-10 4.476562-10 10s4.476562 10 10 10zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m318 384.4375h12c5.523438 0 10-4.476562 10-10s-4.476562-10-10-10h-12c-5.523438 0-10 4.476562-10 10s4.476562 10 10 10zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m444 140.5625c0 5.523438 4.476562 10 10 10s10-4.476562 10-10v-12c0-5.523438-4.476562-10-10-10s-10 4.476562-10 10zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m392 364.4375h-12c-5.523438 0-10 4.476562-10 10s4.476562 10 10 10h12c5.523438 0 10-4.476562 10-10s-4.476562-10-10-10zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m152.0625 68.5h12c5.523438 0 10-4.476562 10-10s-4.476562-10-10-10h-12c-5.523438 0-10 4.476562-10 10s4.476562 10 10 10zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m454 88.5625c5.523438 0 10-4.476562 10-10v-12c0-5.523438-4.476562-10-10-10s-10 4.476562-10 10v12c0 5.523438 4.476562 10 10 10zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m444 264.5625c0 5.523438 4.476562 10 10 10s10-4.476562 10-10v-12c0-5.523438-4.476562-10-10-10s-10 4.476562-10 10zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m432 374.4375c0 5.523438 4.476562 10 10 10h12c5.523438 0 10-4.476562 10-10s-4.476562-10-10-10h-12c-5.523438 0-10 4.476562-10 10zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m444 326.5625c0 5.523438 4.476562 10 10 10s10-4.476562 10-10v-12c0-5.523438-4.476562-10-10-10s-10 4.476562-10 10zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m444 202.5625c0 5.523438 4.476562 10 10 10s10-4.476562 10-10v-12c0-5.523438-4.476562-10-10-10s-10 4.476562-10 10zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m412.0625 68.5c5.523438 0 10-4.476562 10-10s-4.476562-10-10-10h-12c-5.523438 0-10 4.476562-10 10s4.476562 10 10 10zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m194 364.4375c-5.523438 0-10 4.476562-10 10s4.476562 10 10 10h12c5.523438 0 10-4.476562 10-10s-4.476562-10-10-10zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m246 374.4375c0 5.523438 4.476562 10 10 10h12c5.523438 0 10-4.476562 10-10s-4.476562-10-10-10h-12c-5.523438 0-10 4.476562-10 10zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m360.0625 58.5c0-5.523438-4.476562-10-10-10h-12c-5.523438 0-10 4.476562-10 10s4.476562 10 10 10h12c5.523438 0 10-4.476562 10-10zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m99.753906 108.5v80c0 5.523438 4.476563 10 10 10 5.523438 0 10-4.476562 10-10v-30h27.191406v30c0 5.523438 4.476563 10 10 10 5.523438 0 10-4.476562 10-10v-80c0-5.523438-4.476562-10-10-10-5.523437 0-10 4.476562-10 10v30h-27.191406v-30c0-5.523438-4.476562-10-10-10-5.523437 0-10 4.476562-10 10zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m264.074219 188.5c0 5.523438 4.476562 10 10 10 5.523437 0 10-4.476562 10-10v-43.367188l28.578125 48.449219c1.832031 3.101563 5.136718 4.917969 8.613281 4.917969.875 0 1.757813-.113281 2.632813-.351562 4.351562-1.1875 7.367187-5.140626 7.367187-9.648438v-80c0-5.523438-4.476563-10-10-10s-10 4.476562-10 10v43.367188l-28.578125-48.449219c-2.289062-3.882813-6.898438-5.753907-11.246094-4.566407-4.351562 1.1875-7.367187 5.140626-7.367187 9.648438zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m190.800781 198.09375c5.296875 1.5625 10.859375-1.464844 12.421875-6.765625l3.988282-13.523437h20.03125l3.988281 13.523437c1.285156 4.355469 5.269531 7.175781 9.585937 7.175781.9375 0 1.890625-.132812 2.835938-.410156 5.296875-1.5625 8.324218-7.125 6.761718-12.421875l-23.597656-80c-1.253906-4.25-5.15625-7.171875-9.589844-7.171875-4.433593 0-8.335937 2.921875-9.589843 7.171875l-23.597657 80c-1.5625 5.296875 1.464844 10.859375 6.761719 12.421875zm30.542969-40.289062h-8.234375l4.117187-13.957032zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m360.214844 98.5c-5.523438 0-10 4.476562-10 10v80c0 5.523438 4.476562 10 10 10h2.03125c27.570312 0 50-22.429688 50-50s-22.429688-50-50-50zm10 21.074219c12.6875 3.5 22.03125 15.140625 22.03125 28.925781s-9.34375 25.425781-22.03125 28.925781zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m166.945312 324.5v-80c0-4.035156-2.421874-7.675781-6.148437-9.230469-3.722656-1.554687-8.015625-.714843-10.882813 2.121094l-16.5625 16.394531-16.5625-16.394531c-2.867187-2.835937-7.160156-3.675781-10.886718-2.121094-3.722656 1.554688-6.148438 5.195313-6.148438 9.230469v80c0 5.523438 4.476563 10 10 10 5.523438 0 10-4.476562 10-10v-56.03125l6.5625 6.496094c3.898438 3.855468 10.171875 3.855468 14.070313 0l6.5625-6.496094v56.03125c0 5.523438 4.476562 10 10 10 5.523437 0 9.996093-4.476562 9.996093-10zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m395.660156 254.5c5.523438 0 10-4.476562 10-10s-4.476562-10-10-10h-35.445312c-5.523438 0-10 4.476562-10 10v80c0 5.523438 4.476562 10 10 10h35.445312c5.523438 0 10-4.476562 10-10s-4.476562-10-10-10h-25.445312v-20h25.445312c5.523438 0 10-4.476562 10-10s-4.476562-10-10-10h-25.445312v-20zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m190.800781 334.09375c5.296875 1.5625 10.859375-1.46875 12.421875-6.765625l3.988282-13.523437h20.03125l3.988281 13.523437c1.285156 4.355469 5.269531 7.175781 9.585937 7.175781.9375 0 1.890625-.132812 2.835938-.410156 5.296875-1.566406 8.324218-7.125 6.761718-12.421875l-23.597656-80c-1.253906-4.253906-5.15625-7.171875-9.589844-7.171875-4.433593 0-8.335937 2.917969-9.589843 7.171875l-23.597657 80c-1.5625 5.296875 1.464844 10.859375 6.761719 12.421875zm30.542969-40.289062h-8.234375l4.117187-13.957032zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                                 <path
                                   d="m328.6875 284.5c0-27.570312-22.429688-50-50-50h-2.03125c-5.523438 0-10 4.476562-10 10v80c0 5.523438 4.476562 10 10 10h2.03125c27.570312 0 50-22.429688 50-50zm-42.03125 28.925781v-57.851562c12.6875 3.5 22.03125 15.140625 22.03125 28.925781s-9.347656 25.425781-22.03125 28.925781zm0 0"
                                   data-original="#000000"
                                   class="active-path"
                                   data-old_color="#000000"
                                   fill="#3B3B3B"
                                 />
                               </g>
                             </svg>                            
                                 <p>Fatto a mano</p>
                                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background: new 0 0 512 512;" xml:space="preserve" width="20px" height="20px">
                                   <g>
                                     <g>
                                       <g>
                                         <path d="M492,236H276V20c0-11.046-8.954-20-20-20c-11.046,0-20,8.954-20,20v216H20c-11.046,0-20,8.954-20,20s8.954,20,20,20h216    v216c0,11.046,8.954,20,20,20s20-8.954,20-20V276h216c11.046,0,20-8.954,20-20C512,244.954,503.046,236,492,236z" data-original="#3b3b3b" class="active-path scroll" data-old_color="#3b3b3b" fill="$grafiteColor"></path>
                                       </g>
                                     </g>
                                   </g>
                                 </svg>
                               </a>
                               <div class="panel">
                                 <p>
                                 Prodotto realizzato a mano al momento dell'ordine, con cura e passione. Le eventuali piccole imperfezioni o lievi irregolarità certificano ed esaltano l'artigianalità di ogni singolo prodotto, aumentandone il valore e rendendolo un pezzo unico ed esclusivo
                                 </p>
                               </div>
                               <a href="" class="main_category">
<svg
  xmlns="http://www.w3.org/2000/svg"
  height="80"
  viewBox="0 0 512.00061 512"
  width="50"
>
  <g>
    <path
      d="m412 136c0 5.519531 4.480469 10 10 10s10-4.480469 10-10-4.480469-10-10-10-10 4.480469-10 10zm0 0"
      data-original="#000000"
      class="active-path"
      data-old_color="#000000"
      fill="#3B3B3B"
    />
    <path
      d="m452 256c0 12.808594-1.285156 25.59375-3.816406 38-1.105469 5.410156 2.386718 10.691406 7.796875 11.796875.675781.140625 1.347656.207031 2.011719.207031 4.652343 0 8.820312-3.269531 9.789062-8.003906 2.800781-13.722656 4.21875-27.851562 4.21875-42 0-31.488281-6.828125-61.789062-20.300781-90.0625-2.375-4.984375-8.339844-7.101562-13.328125-4.726562-4.988282 2.375-7.101563 8.34375-4.726563 13.332031 12.179688 25.5625 18.355469 52.96875 18.355469 81.457031zm0 0"
      data-original="#000000"
      class="active-path"
      data-old_color="#000000"
      fill="#3B3B3B"
    />
    <path
      d="m306 346h-10v-130c0-5.523438-4.476562-10-10-10h-80c-5.523438 0-10 4.476562-10 10v40c0 5.523438 4.476562 10 10 10h10v80h-10c-5.523438 0-10 4.476562-10 10v40c0 5.523438 4.476562 10 10 10h100c5.523438 0 10-4.476562 10-10v-40c0-5.523438-4.476562-10-10-10zm-10 40h-80v-20h10c5.523438 0 10-4.476562 10-10v-100c0-5.523438-4.476562-10-10-10h-10v-20h60v130c0 5.523438 4.480469 10 10 10h10zm0 0"
      data-original="#000000"
      class="active-path"
      data-old_color="#000000"
      fill="#3B3B3B"
    />
    <path
      d="m256 186c22.054688 0 40-17.945312 40-40s-17.945312-40-40-40-40 17.945312-40 40 17.945312 40 40 40zm0-60c11.027344 0 20 8.972656 20 20s-8.972656 20-20 20-20-8.972656-20-20 8.972656-20 20-20zm0 0"
      data-original="#000000"
      class="active-path"
      data-old_color="#000000"
      fill="#3B3B3B"
    />
    <path
      d="m256 0c-137.976562 0-256 117.800781-256 256 0 47.207031 13.527344 97.410156 36.335938 135.382812l-35.824219 107.457032c-1.195313 3.589844-.261719 7.554687 2.417969 10.230468 2.691406 2.691407 6.660156 3.609376 10.234374 2.414063l107.457032-35.820313c37.96875 22.8125 88.171875 36.335938 135.378906 36.335938 138.011719 0 256-117.816406 256-256 0-138.011719-117.8125-256-256-256zm0 492c-45.285156 0-93.417969-13.363281-128.757812-35.746094-2.503907-1.585937-5.625-2.003906-8.515626-1.039062l-92.914062 30.972656 30.972656-92.914062c.953125-2.851563.570313-5.976563-1.039062-8.515626-22.382813-35.335937-35.746094-83.472656-35.746094-128.757812 0-127.925781 108.074219-236 236-236s236 108.074219 236 236-108.074219 236-236 236zm0 0"
      data-original="#000000"
      class="active-path"
      data-old_color="#000000"
      fill="#3B3B3B"
    />
  </g>
</svg>
  <p>Maggiori Informazioni</p>
  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background: new 0 0 512 512;" xml:space="preserve" width="20px" height="20px">
    <g>
      <g>
        <g>
          <path d="M492,236H276V20c0-11.046-8.954-20-20-20c-11.046,0-20,8.954-20,20v216H20c-11.046,0-20,8.954-20,20s8.954,20,20,20h216    v216c0,11.046,8.954,20,20,20s20-8.954,20-20V276h216c11.046,0,20-8.954,20-20C512,244.954,503.046,236,492,236z" data-original="#3b3b3b" class="active-path scroll" data-old_color="#3b3b3b" fill="$grafiteColor"></path>
        </g>
      </g>
    </g>
  </svg>
</a>
<div class="panel">
  <p>
    ${element.info}
  </p>
</div>
                               <a href="" class="main_category">
                               <svg
  xmlns="http://www.w3.org/2000/svg"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  version="1.1"
  id="Capa_1"
  x="0px"
  y="0px"
  width="50"
  height="50"
  viewBox="0 0 441.732 441.732"
  style="enable-background: new 0 0 441.732 441.732;"
  xml:space="preserve"
>
  <g>
    <g>
      <g>
        <polygon
          points="342.289,32.594 314.181,50.799 311.618,65.33 339.73,47.125   "
          data-original="#000000"
          class="active-path"
          data-old_color="#000000"
          fill="#3B3B3B"
        />
        <polygon
          points="337.032,62.381 308.934,80.581 305.977,97.298 334.086,79.092   "
          data-original="#000000"
          class="active-path"
          data-old_color="#000000"
          fill="#3B3B3B"
        />
        <polygon
          points="347.29,4.257 323.147,0 316.876,35.541 344.976,17.34   "
          data-original="#000000"
          class="active-path"
          data-old_color="#000000"
          fill="#3B3B3B"
        />
        <path
          d="M267.251,155.936c-8.377,0-15.174,6.791-15.174,15.173c0,8.389,6.797,15.18,15.174,15.18c0.389,0,0.762-0.09,1.135-0.112    c3.661,26.969-24.635,51.625-51.435,53.078c-30.184,1.629-42.702-27.605-45.202-53.397c6.709-1.567,11.712-7.56,11.712-14.742    c0-8.378-6.793-15.174-15.174-15.174c-8.377,0-15.173,6.791-15.173,15.174c0,4.646,2.128,8.747,5.416,11.533    c2.22,35.485,22.442,74.765,64.052,69.129c34.83-4.717,65.965-38.285,57.522-72.662c1.457-2.332,2.325-5.054,2.325-8    C282.426,162.732,275.629,155.936,267.251,155.936z"
          data-original="#000000"
          class="active-path"
          data-old_color="#000000"
          fill="#3B3B3B"
        />
        <path
          d="M382.406,426.771c-11.159-43.923-44.178-194.693-0.588-290.595c0.046-0.093,0.052-0.194,0.093-0.29    c0.235-0.562,0.407-1.127,0.533-1.715c0.032-0.145,0.08-0.285,0.107-0.425c0.12-0.728,0.158-1.469,0.12-2.207    c-0.005-0.135-0.05-0.271-0.06-0.4c-0.065-0.593-0.174-1.182-0.355-1.765c-0.061-0.208-0.136-0.407-0.214-0.61    c-0.191-0.509-0.431-1.004-0.711-1.485c-0.064-0.118-0.098-0.257-0.182-0.386c-0.041-0.074-0.112-0.118-0.153-0.192    c-0.08-0.112-0.126-0.235-0.206-0.347l-31.349-42.148c-1.11-1.502-2.648-2.567-4.347-3.215c3.613,8.758,3.007,19.403-1.825,27.519    l10.356,13.929H309.9l18.676-12.099l2.825-15.986l-28.104,18.206l-1.751,9.88h-29.452l14.392-22.696l3.277-5.321l-22.199-14.131    l-53.426-33.877l-16.085,25.366l15.609,50.665h-8.073L172.888,16.29L87.207,42.687l11.575,37.588h-8.981    c-3.648,0-6.994,2.038-8.656,5.289l-21.613,42.148c-0.096,0.187-0.134,0.38-0.225,0.572c-0.112,0.246-0.208,0.493-0.294,0.755    c-0.159,0.466-0.277,0.936-0.356,1.415c-0.043,0.222-0.093,0.441-0.118,0.67c-0.06,0.635-0.071,1.262-0.011,1.888    c0.007,0.084-0.005,0.164,0,0.249c0.086,0.706,0.261,1.406,0.495,2.082c0.023,0.058,0.023,0.113,0.039,0.17    c46.047,125.29,13.634,255.43,2.599,292.333c-0.631,2.122-1.807,6.085,1.166,10.024c1.834,2.435,4.72,3.863,7.762,3.863h0.128    c2.638-0.033,4.566-0.939,6-2.209h291.263c1.385,1.021,3.192,1.732,5.565,1.732c3.006,0,5.841-1.388,7.685-3.759    C384.166,433.721,383.271,430.186,382.406,426.771z M95.752,99.732h9.028l6.99,22.695H84.102L95.752,99.732z M84.046,420.068    c12.849-48.63,35.138-163.794-2.197-278.18h276.61c-34.768,93.836-10.332,224.637,2.278,278.18H84.046z"
          data-original="#000000"
          class="active-path"
          data-old_color="#000000"
          fill="#3B3B3B"
        />
      </g>
    </g>
  </g>
</svg>
                                 <p>Cosa è incluso</p>
                                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background: new 0 0 512 512;" xml:space="preserve" width="20px" height="20px">
                                   <g>
                                     <g>
                                       <g>
                                         <path d="M492,236H276V20c0-11.046-8.954-20-20-20c-11.046,0-20,8.954-20,20v216H20c-11.046,0-20,8.954-20,20s8.954,20,20,20h216    v216c0,11.046,8.954,20,20,20s20-8.954,20-20V276h216c11.046,0,20-8.954,20-20C512,244.954,503.046,236,492,236z" data-original="#3b3b3b" class="active-path scroll" data-old_color="#3b3b3b" fill="$grafiteColor"></path>
                                       </g>
                                     </g>
                                   </g>
                                 </svg>
                               </a>
                               <div class="panel">
                                 <p>
                                   ${element.incluso}
                                 </p>
                               </div>
                               <a href="" class="main_category">
                               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background: new 0 0 512 512;" xml:space="preserve" width ="50" height="50" class="">
               <g>
                 <g>
                   <g>
                     <path d="M503.673,243.579c-5.323-6.344-12.584-10.467-20.908-12.008l-3.91-53.066c3.857-0.037,7.146-2.815,7.818-6.622    l1.508-8.551c1.616-9.166-0.638-18.214-6.185-24.823c-5.54-6.602-13.651-10.239-22.84-10.239h-58.854l2.865-16.245    c0.813-4.614-0.365-9.221-3.231-12.637c-2.838-3.382-7.105-5.322-11.707-5.322H98.524c-8.567,0-16.453,6.665-17.954,15.176    L54.939,254.609c-0.768,4.36,2.143,8.518,6.503,9.286c0.47,0.083,0.938,0.123,1.401,0.123c3.817,0,7.2-2.737,7.885-6.626    L96.36,112.025c0.169-0.957,1.401-1.927,2.163-1.927l288.702,0.001l-25.481,144.51c-0.769,4.36,2.142,8.518,6.503,9.286    c4.355,0.767,8.518-2.143,9.286-6.503l1.848-10.479h95.866c0.003,0,0.007,0,0.011,0s0.008,0,0.011,0    c6.626,0.001,12.351,2.476,16.122,6.969c2.595,3.093,4.109,6.943,4.473,11.202h-26.629c-3.891,0-7.219,2.793-7.895,6.625    l-3.015,17.102c-1.215,6.89,0.501,13.717,4.707,18.73c4.148,4.945,10.445,7.78,17.274,7.78h7.548l-6.22,35.273h-21.165    c-1.393-7.055-4.442-13.544-9.063-19.049c-8.199-9.773-20.265-15.154-33.972-15.154c-21.979,0-43.184,14.38-53.111,34.204h-3.223    l10.403-58.999c0.768-4.36-2.142-8.518-6.503-9.286c-4.358-0.77-8.518,2.142-9.286,6.503l-10.894,61.783H196.418    c-1.393-7.055-4.442-13.543-9.063-19.049c-8.2-9.773-20.265-15.154-33.973-15.154c-21.979,0-43.184,14.38-53.111,34.204    l-45.978-0.001l3.204-18.17h36.029c4.427,0,8.017-3.589,8.017-8.017c0-4.427-3.589-8.017-8.017-8.017H8.017    c-4.427,0-8.017,3.589-8.017,8.017c0,4.427,3.589,8.017,8.017,8.017h33.201l-2.865,16.245c-0.813,4.614,0.365,9.221,3.231,12.637    c2.838,3.382,7.105,5.322,11.707,5.322h41.774c-2.173,13.599,1.093,26.41,9.268,36.151c8.2,9.773,20.265,15.154,33.973,15.154    c27.284,0,53.387-22.151,58.188-49.38c0.113-0.645,0.202-1.286,0.292-1.926h162.331c-2.174,13.598,1.092,26.409,9.268,36.151    c8.2,9.773,20.265,15.154,33.973,15.154c27.284,0,53.387-22.151,58.188-49.38c0.113-0.645,0.202-1.286,0.292-1.926h27.525    c3.891,0,7.219-2.793,7.895-6.625l15.078-85.51C513.382,262.886,510.661,251.907,503.673,243.579z M382.21,230.883l9.235-52.375    h71.336l3.859,52.375H382.21z M472.391,160.549l-0.34,1.926h-77.78l3.204-18.171h61.681c4.367,0,8.117,1.602,10.557,4.511    C472.243,151.829,473.195,155.995,472.391,160.549z M180.705,365.773c-3.512,19.923-22.533,36.13-42.399,36.13    c-8.886,0-16.59-3.348-21.691-9.426c-5.248-6.255-7.248-14.749-5.631-23.919c3.513-19.923,22.533-36.13,42.399-36.13    c8.886,0,16.59,3.348,21.691,9.427C180.322,348.108,182.322,356.603,180.705,365.773z M444.756,365.773    c-3.513,19.923-22.533,36.13-42.399,36.13c-8.886,0-16.59-3.348-21.691-9.427c-5.248-6.255-7.248-14.749-5.631-23.919    c3.512-19.923,22.533-36.13,42.399-36.13c8.885,0,16.59,3.348,21.691,9.427C444.373,348.108,446.373,356.603,444.756,365.773z     M490.681,299.292h-10.375v-0.001c-2.139,0-3.865-0.71-4.992-2.052c-1.169-1.394-1.596-3.397-1.2-5.64l1.848-10.477h17.923    L490.681,299.292z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3b3b3b"></path>
                   </g>
                 </g>
                 <g>
                   <g>
                     <path d="M159.06,355.919c-2.838-3.382-7.105-5.322-11.708-5.322c-8.567,0-16.453,6.665-17.954,15.176    c-0.813,4.614,0.365,9.221,3.231,12.637c2.838,3.382,7.105,5.322,11.707,5.322c8.567,0,16.453-6.666,17.954-15.175    C163.104,363.942,161.927,359.336,159.06,355.919z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3b3b3b"></path>
                   </g>
                 </g>
                 <g>
                   <g>
                     <path d="M423.111,355.919c-2.839-3.382-7.106-5.322-11.707-5.322c-8.567,0-16.453,6.665-17.953,15.175    c-0.813,4.615,0.363,9.221,3.23,12.638c2.838,3.382,7.105,5.322,11.707,5.322c8.567,0,16.453-6.666,17.954-15.175    C427.156,363.942,425.978,359.336,423.111,355.919z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3b3b3b"></path>
                   </g>
                 </g>
                 <g>
                   <g>
                     <path d="M323.374,316.393H221.791c-4.427,0-8.017,3.589-8.017,8.017c0,4.427,3.589,8.017,8.017,8.017h101.583    c4.427,0,8.017-3.589,8.017-8.017C331.39,319.982,327.801,316.393,323.374,316.393z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3b3b3b"></path>
                   </g>
                 </g>
                 <g>
                   <g>
                     <path d="M179.036,282.189H31.15c-4.427,0-8.017,3.589-8.017,8.017c0,4.427,3.588,8.017,8.017,8.017h147.886    c4.427,0,8.017-3.589,8.017-8.017C187.053,285.778,183.464,282.189,179.036,282.189z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3b3b3b"></path>
                   </g>
                 </g>
                 <g>
                   <g>
                     <path d="M332.127,249.936l-29.68-25.653c-3.35-2.894-8.412-2.527-11.308,0.823c-2.896,3.35-2.527,8.412,0.823,11.308    l13.388,11.572H102.077c-4.427,0-8.017,3.589-8.017,8.017c0,4.427,3.589,8.017,8.017,8.017h198.189l-16.535,10.954    c-3.692,2.444-4.701,7.419-2.256,11.11c1.542,2.329,4.092,3.59,6.69,3.59c1.52,0,3.058-0.432,4.42-1.335l38.727-25.653    c2.092-1.384,3.413-3.668,3.573-6.172C335.045,254.009,334.025,251.575,332.127,249.936z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3b3b3b"></path>
                   </g>
                 </g>
               </g>
             </svg>
                                 <p>Costi di spedizione</p>
                                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background: new 0 0 512 512;" xml:space="preserve" width="20px" height="20px">
                                   <g>
                                     <g>
                                       <g>
                                         <path d="M492,236H276V20c0-11.046-8.954-20-20-20c-11.046,0-20,8.954-20,20v216H20c-11.046,0-20,8.954-20,20s8.954,20,20,20h216    v216c0,11.046,8.954,20,20,20s20-8.954,20-20V276h216c11.046,0,20-8.954,20-20C512,244.954,503.046,236,492,236z" data-original="#3b3b3b" class="active-path scroll" data-old_color="#3b3b3b" fill="$grafiteColor"></path>
                                       </g>
                                     </g>
                                   </g>
                                 </svg>
                               </a>
                               <div class="panel">
                                 <p>
                                     ${element.spedizione}
                                 </p>
                               </div>
                               <a href="" class="main_category">
                               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512.001 512.001" style="enable-background: new 0 0 512.001 512.001;" xml:space="preserve" width  ="50" height="50">
                               <g>
                                 <g>
                                   <g>
                                     <path d="M407.04,385.22c-4.539-3.148-10.77-2.016-13.915,2.523c-3.048,4.4-6.502,8.678-10.271,12.717    c-2.525,2.709-5.999,5.975-9.779,9.191c-4.206,3.58-4.714,9.891-1.135,14.098c1.978,2.324,4.791,3.518,7.621,3.518    c2.291,0,4.594-0.783,6.477-2.385c4.41-3.754,8.369-7.482,11.443-10.783c4.422-4.738,8.488-9.773,12.084-14.965    C412.71,394.595,411.58,388.365,407.04,385.22z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3b3b3b"></path>
                                   </g>
                                 </g>
                                 <g>
                                   <g>
                                     <path d="M363.474,430.058c-2.346-5-8.298-7.152-13.3-4.807l-0.313,0.141c-5.053,2.229-7.341,8.133-5.11,13.186    c1.65,3.74,5.312,5.965,9.153,5.965c1.349,0,2.72-0.275,4.032-0.854c0.244-0.107,0.486-0.217,0.729-0.33    C363.665,441.013,365.818,435.06,363.474,430.058z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3b3b3b"></path>
                                   </g>
                                 </g>
                                 <g>
                                   <g>
                                     <path d="M482.126,26.001H29.875C13.401,26.001,0,39.404,0,55.876v280.252c0,16.471,13.401,29.873,29.875,29.873h210.586    c8.927,37.77,29.114,64.52,46.757,81.658C312.97,472.677,342.49,486,353.899,486c11.408,0,40.928-13.322,66.681-38.34    c17.643-17.139,37.831-43.889,46.757-81.658h14.789c16.473,0,29.875-13.402,29.875-29.873V55.876    C512.001,39.404,498.599,26.001,482.126,26.001z M452,325.019c0.001,52.688-24.32,87.637-44.724,107.678    c-24.203,23.773-49.03,33.02-53.377,33.301c-4.348-0.281-29.176-9.527-53.379-33.301c-20.402-20.041-44.723-54.99-44.723-107.678    v-49.246c34.43-9.957,66.646-23.584,98.105-41.492c30.75,17.453,64.41,31.686,98.098,41.486V325.019z M492.001,336.128    c0,5.444-4.431,9.873-9.875,9.873h-11.311c0.766-6.701,1.186-13.689,1.186-20.982v-56.828c0-4.514-3.024-8.467-7.379-9.65    c-36.313-9.861-72.854-25.227-105.672-44.436c-3.12-1.826-6.982-1.824-10.104,0.002c-33.748,19.754-68.313,34.287-105.67,44.434    c-4.355,1.184-7.379,5.137-7.379,9.65v56.828c0,7.293,0.419,14.281,1.186,20.982H29.875c-5.445,0-9.875-4.43-9.875-9.873V165.999    h472.001V336.128z M492.001,146H20v-39.998h472.001V146z M492.001,86.001H20V55.876c0-5.445,4.43-9.875,9.875-9.875h452.251    c5.444,0,9.875,4.43,9.875,9.875V86.001z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3b3b3b"></path>
                                   </g>
                                 </g>
                                 <g>
                                   <g>
                                     <path d="M119.997,236.003c-6.029,0-11.982,1.219-17.492,3.543c-5.383-2.281-11.299-3.543-17.503-3.543    c-24.813,0-45.001,20.186-45.001,44.998s20.187,45,45.001,45c6.204,0,12.12-1.262,17.502-3.543    c5.511,2.324,11.464,3.543,17.493,3.543c24.814,0,45.002-20.188,45.002-45S144.811,236.003,119.997,236.003z M85.002,306.001    c-13.786,0-25.001-11.214-25.001-25c0-13.785,11.215-24.998,25.001-24.998c13.784,0,24.999,11.213,24.999,24.998    C110.001,294.786,98.786,306.001,85.002,306.001z M122.476,305.879c4.75-7.131,7.525-15.686,7.525-24.877    c0-9.191-2.775-17.744-7.524-24.875c12.625,1.248,22.521,11.928,22.521,24.875C144.998,293.949,135.102,304.63,122.476,305.879z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3b3b3b"></path>
                                   </g>
                                 </g>
                                 <g>
                                   <g>
                                     <path d="M101.436,191.001H49.999c-5.523,0-10,4.477-10,10c0,5.522,4.477,10,10,10h51.437c5.523,0,10-4.479,10-10    C111.436,195.478,106.959,191.001,101.436,191.001z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3b3b3b"></path>
                                   </g>
                                 </g>
                                 <g>
                                   <g>
                                     <path d="M130.996,191.001h-0.474c-5.523,0-10,4.477-10,10c0,5.522,4.477,10,10,10h0.474c5.522,0,10-4.479,10-10    C140.996,195.478,136.518,191.001,130.996,191.001z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3b3b3b"></path>
                                   </g>
                                 </g>
                                 <g>
                                   <g>
                                     <path d="M408.79,302.026c-3.903-3.903-10.234-3.905-14.141-0.001l-53.477,53.473l-28.023-28.025    c-3.906-3.902-10.238-3.904-14.143,0c-3.905,3.906-3.906,10.238,0,14.143l35.095,35.096c1.953,1.953,4.512,2.93,7.071,2.93    s5.119-0.977,7.07-2.93l60.547-60.543C412.695,312.265,412.695,305.933,408.79,302.026z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3b3b3b"></path>
                                   </g>
                                 </g>
                               </g>
                             </svg>
                                 <p>Pagamenti</p>
                                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background: new 0 0 512 512;" xml:space="preserve" width="20px" height="20px">
                                   <g>
                                     <g>
                                       <g>
                                         <path d="M492,236H276V20c0-11.046-8.954-20-20-20c-11.046,0-20,8.954-20,20v216H20c-11.046,0-20,8.954-20,20s8.954,20,20,20h216    v216c0,11.046,8.954,20,20,20s20-8.954,20-20V276h216c11.046,0,20-8.954,20-20C512,244.954,503.046,236,492,236z" data-original="#3b3b3b" class="active-path scroll" data-old_color="#3b3b3b" fill="$grafiteColor"></path>
                                       </g>
                                     </g>
                                   </g>
                                 </svg>
                               </a>
                               <div class="panel">
                                 <p>
                                     Pagamenti sicuri e protetti con Paypal. Non è necessario creare un account, è possibile pagare direttamente con carte di credito Visa, Mastercard e American Express
                                 </p>
                               </div>
                               <a href="" class="main_category">
                               <svg xmlns="http://www.w3.org/2000/svg" height="50" version="1.1" viewBox="-42 0 511 511.99956" width  ="50">
               <g>
                 <g id="surface1">
                   <path d="M 423.15625 277.039062 L 376.34375 251.3125 C 373.082031 249.519531 369.105469 249.648438 365.96875 251.648438 L 317.027344 282.792969 L 252.609375 282.792969 C 243.4375 280.21875 232.898438 278.257812 221.265625 277.722656 C 221.042969 260.730469 222.285156 215.085938 239.613281 173.929688 C 245.5625 176.265625 254.683594 178.894531 266.050781 179.15625 C 266.679688 179.171875 267.308594 179.179688 267.933594 179.179688 C 300.460938 179.179688 326.625 159.71875 342.957031 143.210938 C 392.226562 93.425781 409.511719 14.101562 410.222656 10.75 C 410.226562 10.726562 410.230469 10.703125 410.234375 10.679688 C 410.269531 10.511719 410.292969 10.339844 410.316406 10.167969 C 410.324219 10.09375 410.339844 10.019531 410.347656 9.945312 C 410.367188 9.761719 410.371094 9.574219 410.378906 9.390625 C 410.378906 9.324219 410.386719 9.261719 410.386719 9.195312 C 410.386719 9.015625 410.375 8.832031 410.359375 8.648438 C 410.355469 8.582031 410.355469 8.511719 410.347656 8.441406 C 410.332031 8.28125 410.304688 8.128906 410.277344 7.96875 C 410.261719 7.875 410.25 7.777344 410.230469 7.683594 C 410.207031 7.558594 410.171875 7.4375 410.136719 7.3125 C 410.105469 7.1875 410.078125 7.0625 410.039062 6.9375 C 410.03125 6.925781 410.03125 6.910156 410.027344 6.894531 C 409.992188 6.789062 409.949219 6.691406 409.910156 6.589844 C 409.867188 6.46875 409.824219 6.347656 409.773438 6.230469 C 409.710938 6.082031 409.640625 5.941406 409.566406 5.800781 C 409.527344 5.71875 409.492188 5.636719 409.445312 5.558594 C 409.347656 5.378906 409.234375 5.203125 409.121094 5.03125 C 409.097656 5 409.082031 4.964844 409.058594 4.933594 C 409.058594 4.929688 409.054688 4.925781 409.050781 4.921875 C 408.617188 4.292969 408.09375 3.742188 407.503906 3.285156 C 407.441406 3.234375 407.378906 3.195312 407.316406 3.148438 C 407.179688 3.050781 407.046875 2.953125 406.90625 2.863281 C 406.808594 2.800781 406.707031 2.75 406.609375 2.691406 C 406.496094 2.628906 406.382812 2.558594 406.265625 2.5 C 406.15625 2.445312 406.042969 2.398438 405.933594 2.347656 C 405.820312 2.300781 405.707031 2.246094 405.59375 2.203125 C 405.480469 2.160156 405.363281 2.121094 405.25 2.085938 C 405.128906 2.042969 405.007812 2.003906 404.882812 1.96875 C 404.773438 1.9375 404.660156 1.914062 404.546875 1.886719 C 404.414062 1.859375 404.28125 1.828125 404.148438 1.804688 C 404.109375 1.796875 404.070312 1.785156 404.035156 1.78125 C 402.457031 1.535156 364.9375 -4.074219 324.398438 5.972656 C 291.226562 14.195312 248.816406 36.726562 229.28125 75.292969 C 215.816406 101.875 215.296875 131.59375 227.722656 163.667969 C 220.925781 178.71875 216.238281 194.304688 213.023438 209.101562 C 209 195.9375 203.253906 181.445312 195.113281 168.164062 C 200.566406 144.667969 197.304688 123.949219 185.363281 106.554688 C 167.921875 81.152344 135.941406 69.25 111.832031 66.585938 C 82.53125 63.34375 56.777344 70.785156 55.695312 71.105469 C 55.660156 71.117188 55.625 71.132812 55.585938 71.144531 C 55.457031 71.183594 55.332031 71.230469 55.207031 71.277344 C 55.097656 71.320312 54.984375 71.359375 54.878906 71.402344 C 54.761719 71.453125 54.652344 71.507812 54.539062 71.5625 C 54.425781 71.617188 54.3125 71.671875 54.207031 71.730469 C 54.101562 71.789062 54.003906 71.851562 53.902344 71.914062 C 53.792969 71.980469 53.683594 72.042969 53.578125 72.117188 C 53.476562 72.183594 53.378906 72.261719 53.28125 72.335938 C 53.183594 72.410156 53.085938 72.480469 52.992188 72.558594 C 52.871094 72.660156 52.757812 72.773438 52.640625 72.882812 C 52.582031 72.941406 52.515625 72.996094 52.457031 73.054688 C 51.929688 73.589844 51.488281 74.207031 51.136719 74.886719 C 51.136719 74.890625 51.132812 74.890625 51.132812 74.894531 C 51.117188 74.925781 51.105469 74.957031 51.089844 74.988281 C 50.996094 75.179688 50.910156 75.371094 50.832031 75.570312 C 50.796875 75.652344 50.773438 75.734375 50.746094 75.816406 C 50.691406 75.96875 50.636719 76.121094 50.59375 76.277344 C 50.558594 76.398438 50.535156 76.519531 50.507812 76.644531 C 50.484375 76.753906 50.453125 76.859375 50.433594 76.96875 C 50.429688 76.984375 50.429688 77.003906 50.425781 77.019531 C 50.40625 77.140625 50.394531 77.265625 50.378906 77.386719 C 50.363281 77.519531 50.34375 77.652344 50.332031 77.78125 C 50.328125 77.875 50.328125 77.964844 50.324219 78.054688 C 50.320312 78.21875 50.3125 78.386719 50.320312 78.550781 C 50.320312 78.613281 50.328125 78.675781 50.332031 78.738281 C 50.34375 78.929688 50.355469 79.117188 50.382812 79.304688 C 50.390625 79.359375 50.402344 79.417969 50.410156 79.476562 C 50.441406 79.667969 50.472656 79.855469 50.519531 80.039062 C 50.535156 80.105469 50.554688 80.171875 50.574219 80.238281 C 50.621094 80.414062 50.671875 80.585938 50.726562 80.753906 C 50.734375 80.777344 50.738281 80.796875 50.746094 80.816406 C 51.554688 83.085938 70.984375 136.746094 110.027344 166.910156 C 121.640625 175.882812 138.945312 185.757812 158.847656 185.757812 C 161.863281 185.757812 164.941406 185.53125 168.0625 185.039062 C 175 183.949219 180.554688 181.8125 184.5625 179.808594 C 201.695312 210.148438 206.273438 246.988281 206.914062 252.882812 C 206.207031 263.390625 206.140625 272.035156 206.234375 277.804688 C 195.171875 278.425781 185.121094 280.324219 176.328125 282.792969 L 111.910156 282.792969 L 62.972656 251.648438 C 59.832031 249.652344 55.855469 249.523438 52.59375 251.3125 L 5.78125 277.039062 C 2.617188 278.777344 0.648438 281.96875 0.507812 285.574219 C 0.367188 289.179688 2.089844 292.515625 5.109375 294.492188 L 67.457031 335.289062 L 67.457031 359.222656 C 67.457031 363.363281 70.816406 366.722656 74.957031 366.722656 C 79.097656 366.722656 82.457031 363.363281 82.457031 359.222656 L 82.457031 338.730469 L 174.972656 338.730469 L 174.972656 384.449219 C 174.972656 387.828125 176.636719 390.988281 179.417969 392.90625 C 182.207031 394.820312 185.753906 395.238281 188.910156 394.027344 L 213.722656 384.5 L 238.539062 394.027344 C 239.726562 394.484375 240.976562 394.710938 242.214844 394.710938 C 244.261719 394.710938 246.292969 394.097656 248.027344 392.90625 C 250.8125 390.992188 252.476562 387.828125 252.476562 384.449219 L 252.476562 338.730469 L 346.480469 338.730469 L 346.480469 488.011719 C 346.480469 492.96875 342.445312 497 337.492188 497 L 91.445312 497 C 86.488281 497 82.457031 492.96875 82.457031 488.011719 L 82.457031 394.121094 C 82.457031 389.980469 79.097656 386.621094 74.957031 386.621094 C 70.816406 386.621094 67.457031 389.980469 67.457031 394.121094 L 67.457031 488.011719 C 67.457031 501.238281 78.21875 512 91.445312 512 L 337.492188 512 C 350.71875 512 361.480469 501.238281 361.480469 488.011719 L 361.480469 335.289062 L 423.828125 294.492188 C 426.847656 292.515625 428.570312 289.183594 428.429688 285.574219 C 428.289062 281.96875 426.320312 278.777344 423.15625 277.039062 Z M 242.660156 82.070312 C 259.925781 47.984375 299.425781 27.617188 328.007812 20.53125 C 341.957031 17.074219 355.675781 15.640625 367.429688 15.203125 C 334.046875 31.089844 304.464844 53.757812 279.191406 82.886719 C 276.476562 86.015625 276.8125 90.753906 279.941406 93.46875 C 281.359375 94.699219 283.109375 95.300781 284.851562 95.300781 C 286.949219 95.300781 289.035156 94.425781 290.519531 92.71875 C 318.917969 59.984375 353.035156 35.890625 392.078125 20.949219 C 385.078125 45.292969 366.894531 97.699219 332.296875 132.664062 C 293.226562 172.144531 259.363281 165.46875 245.933594 160.359375 C 253.105469 145.957031 261.09375 132.300781 269.734375 119.683594 C 272.074219 116.265625 271.203125 111.601562 267.785156 109.257812 C 264.371094 106.917969 259.703125 107.789062 257.359375 111.207031 C 250.1875 121.675781 243.460938 132.835938 237.246094 144.503906 C 231.3125 121.867188 233.113281 100.921875 242.660156 82.070312 Z M 119.199219 155.039062 C 94.746094 136.148438 78.304688 105.941406 70.371094 88.792969 C 92.925781 94.789062 113.820312 105.21875 132.636719 119.933594 C 148.84375 132.605469 163.550781 148.449219 176.441406 167.082031 C 166.878906 171.375 146.246094 175.933594 119.199219 155.039062 Z M 182.578125 149.992188 C 162.996094 124.097656 134.253906 96.542969 94.09375 80.804688 C 99.210938 80.707031 104.636719 80.882812 110.183594 81.492188 C 130.039062 83.691406 158.402344 93.792969 172.996094 115.046875 C 179.9375 125.15625 183.128906 136.867188 182.578125 149.992188 Z M 374.019531 264.300781 C 374.023438 264.300781 374.023438 264.300781 374.023438 264.300781 C 374.019531 264.300781 374.015625 264.304688 374.011719 264.304688 Z M 54.917969 264.300781 L 54.921875 264.304688 C 54.921875 264.304688 54.917969 264.300781 54.914062 264.300781 C 54.914062 264.300781 54.914062 264.300781 54.917969 264.300781 Z M 73.523438 321.332031 L 20.023438 286.324219 L 57.320312 265.832031 L 98.4375 291.996094 Z M 115.636719 323.734375 L 91.164062 323.734375 L 113.199219 297.792969 L 141.660156 297.792969 C 128.3125 306 119.160156 315.191406 115.636719 323.734375 Z M 237.476562 377.554688 L 217.40625 369.847656 C 215.03125 368.933594 212.417969 368.933594 210.046875 369.84375 L 189.96875 377.554688 L 189.96875 338.730469 L 237.476562 338.730469 Z M 244.976562 323.734375 L 133.285156 323.734375 C 136.847656 319.589844 142.898438 314.3125 152.335938 308.890625 C 158.101562 305.574219 167.277344 301.042969 179.101562 297.59375 C 179.367188 297.53125 179.625 297.457031 179.878906 297.367188 C 189.621094 294.59375 201.117188 292.578125 213.960938 292.578125 L 214.972656 292.578125 C 227.820312 292.578125 239.3125 294.59375 249.054688 297.367188 C 249.308594 297.457031 249.570312 297.53125 249.835938 297.59375 C 261.65625 301.042969 270.835938 305.574219 276.601562 308.890625 C 286.039062 314.3125 292.085938 319.589844 295.652344 323.734375 Z M 313.300781 323.734375 C 309.777344 315.191406 300.625 306 287.273438 297.792969 L 315.738281 297.792969 L 337.769531 323.734375 Z M 355.414062 321.332031 L 330.496094 291.996094 L 371.617188 265.832031 L 408.910156 286.324219 Z M 355.414062 321.332031 " data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3b3b3b"></path>
                 </g>
               </g>
             </svg>
                                 <p>Packaging</p>
                                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background: new 0 0 512 512;" xml:space="preserve" width="20px" height="20px">
                                   <g>
                                     <g>
                                       <g>
                                         <path d="M492,236H276V20c0-11.046-8.954-20-20-20c-11.046,0-20,8.954-20,20v216H20c-11.046,0-20,8.954-20,20s8.954,20,20,20h216    v216c0,11.046,8.954,20,20,20s20-8.954,20-20V276h216c11.046,0,20-8.954,20-20C512,244.954,503.046,236,492,236z" data-original="#3b3b3b" class="active-path scroll" data-old_color="#3b3b3b" fill="$grafiteColor"></path>
                                       </g>
                                     </g>
                                   </g>
                                 </svg>
                               </a>
                               <div class="panel">
                                 <p>
                                     I nostri packaging sono in cartone riciclato, progettati per spedizioni sia con piante che senza piante, utilizzando imballaggi e protezioni differenti per ogni singolo prodotto. Sempre inclusi nei costi di spedizione
                                 </p>
                               </div>
                               ${
                                 element['affitto']
                                   ? `<a href="" class="main_category">
                                   <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512.001 512.001" style="enable-background: new 0 0 512.001 512.001;" xml:space="preserve" width  ="50" height="50">
                                   <g>
                                     <g>
                                       <g>
                                         <path d="M477.922,174.683H320.166L289.351,83.38c-0.862-17.65-15.489-31.744-33.35-31.744c-17.86,0-32.488,14.094-33.35,31.743    l-30.817,91.302H34.079C15.288,174.683,0,189.97,0,208.762v217.524c0,18.792,15.288,34.079,34.079,34.079h36.413    c5.124,0,9.276-4.153,9.276-9.276s-4.152-9.276-9.276-9.276H34.079c-8.562,0-15.528-6.966-15.528-15.528V208.762    c0-8.562,6.966-15.528,15.528-15.528h164.385c0.01,0,0.02,0.003,0.03,0.003s0.021-0.003,0.031-0.003h114.95    c0.01,0,0.021,0.003,0.031,0.003s0.02-0.003,0.03-0.003h164.386c8.562,0,15.528,6.966,15.528,15.528v217.524    c0,8.562-6.966,15.528-15.528,15.528H100.175c-5.124,0-9.276,4.153-9.276,9.276s4.152,9.276,9.276,9.276h377.747    c18.79,0,34.079-15.288,34.079-34.079V208.762C512,189.97,496.712,174.683,477.922,174.683z M256,70.189    c8.182,0,14.84,6.658,14.84,14.84s-6.658,14.84-14.84,14.84s-14.84-6.658-14.84-14.84S247.818,70.189,256,70.189z     M211.413,174.683l21.977-65.109c5.953,5.488,13.895,8.847,22.609,8.847s16.658-3.359,22.609-8.847l21.975,65.109H211.413z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3B3B3B"></path>
                                       </g>
                                     </g>
                                     <g>
                                       <g>
                                         <path d="M323.054,263.051c-5.124,0-9.276,4.153-9.276,9.276v58.9L272.543,267.3c-2.231-3.457-6.472-5.032-10.416-3.87    c-3.945,1.161-6.655,4.784-6.655,8.897v90.392c0,5.123,4.152,9.276,9.276,9.276s9.276-4.153,9.276-9.276v-58.899l41.235,63.926    c1.74,2.697,4.703,4.248,7.795,4.248c0.872,0,1.754-0.124,2.621-0.378c3.945-1.161,6.655-4.784,6.655-8.897v-90.392    C332.33,267.205,328.178,263.051,323.054,263.051z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3B3B3B"></path>
                                       </g>
                                     </g>
                                     <g>
                                       <g>
                                         <path d="M116.94,326.22c11.333-5.266,19.212-16.75,19.212-30.047c0-18.262-14.857-33.12-33.118-33.12H83.326    c-5.124,0-9.276,4.153-9.276,9.276v90.392c0,5.123,4.152,9.276,9.276,9.276s9.276-4.153,9.276-9.276v-33.429h4.905l20.486,37.844    c1.678,3.102,4.87,4.862,8.166,4.862c1.49,0,3.004-0.361,4.407-1.12c4.506-2.439,6.18-8.068,3.741-12.573L116.94,326.22z     M103.058,310.738c-0.021,0-0.041,0.001-0.062,0.001H92.602c0,0,0-29.136,0-29.136h10.432c8.032,0,14.567,6.535,14.567,14.568    C117.601,304.195,111.078,310.724,103.058,310.738z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3B3B3B"></path>
                                       </g>
                                     </g>
                                     <g>
                                       <g>
                                         <path d="M220.3,327.041c5.124,0,9.276-4.153,9.276-9.276c0-5.123-4.152-9.276-9.276-9.276h-33.737v-26.885H220.3v-0.001    c5.124,0,9.276-4.153,9.276-9.276c0-5.123-4.152-9.276-9.276-9.276h-43.013c-5.124,0-9.276,4.153-9.276,9.276v90.392    c0,5.123,4.152,9.276,9.276,9.276H220.3c5.124,0,9.276-4.153,9.276-9.276c0-5.123-4.152-9.276-9.276-9.276h-33.737v-26.403H220.3z    " data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3B3B3B"></path>
                                       </g>
                                     </g>
                                     <g>
                                       <g>
                                         <path d="M395.22,337.319c-5.124,0-9.276,4.153-9.276,9.276v16.126c0,5.123,4.152,9.276,9.276,9.276s9.276-4.153,9.276-9.276    v-16.126C404.496,341.472,400.343,337.319,395.22,337.319z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3B3B3B"></path>
                                       </g>
                                     </g>
                                     <g>
                                       <g>
                                         <path d="M428.674,263.051h-66.908c-5.124,0-9.276,4.153-9.276,9.276c0,5.123,4.152,9.276,9.276,9.276h24.179v40.255    c0,5.123,4.152,9.276,9.276,9.276s9.276-4.153,9.276-9.276v-40.255h24.178c5.124,0,9.276-4.153,9.276-9.276    C437.949,267.205,433.797,263.051,428.674,263.051z" data-original="#000000" class="active-path scroll" data-old_color="#000000" fill="#3B3B3B"></path>
                                       </g>
                                     </g>
                                   </g>
                                 </svg>
                                   <p>Noleggio</p>
                                   <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background: new 0 0 512 512;" xml:space="preserve" width="20px" height="20px">
                                     <g>
                                       <g>
                                         <g>
                                           <path d="M492,236H276V20c0-11.046-8.954-20-20-20c-11.046,0-20,8.954-20,20v216H20c-11.046,0-20,8.954-20,20s8.954,20,20,20h216    v216c0,11.046,8.954,20,20,20s20-8.954,20-20V276h216c11.046,0,20-8.954,20-20C512,244.954,503.046,236,492,236z" data-original="#3b3b3b" class="active-path scroll" data-old_color="#3b3b3b" fill="$grafiteColor"></path>
                                         </g>
                                       </g>
                                     </g>
                                   </svg>
                                 </a>
                                 <div class="panel">
                                 <p>
                                 Questo prodotto è disponibile anche per il servizio di noleggio. Per maggiori informazioni <a href="https://neasy-jungle-90f4c.firebaseapp.com/src/assets/html/noleggio.html">clicca qui</a>
                             </p>
                                 </div>`
                                   : ''
                               }
                             </div>
                           </section>`;
          dettaglioInfo.innerHTML += htmlDetailInfo;
        }

        productDetails.innerHTML += htmlDetailProduct;
      });
    })
    // ${
    //   element['prezzo'].length > 1
    //     ? '<button>Senza piante</button>'
    //     : '<button>Con piante</button>'
    // }
    .catch((err) => alert(err));
}

fetch(`https://neasy-jungle-1.herokuapp.com/products`)
  .then((response) => response.json())
  .then((ele) => {
    for (var i = 0; i < 4; i++) {
      const randomChoice = ele[~~(Math.random() * ele.length)];
      randomSample.push(randomChoice);
    }

    const sortedRandomSample = randomSample.sort(function (a, b) {
      return parseInt(a) - parseInt(b);
    });

    const htmlProdottiCorrelati = `
    <section class="nj_homepage_section3">
        <h4>
        Prodotti correlati
        </h4>
      </section>
      <div class="nj_search_section2">
        ${sortedRandomSample
          .map((el) => {
            return `
            <article>
              <a href="../html/dettaglio.html" data-search="${el.titolo}">
                <img src="${el.foto[0]}" alt="${el.titolo}" />
                <h3><strong>${el.titolo}</strong></h3>
                <p>da € ${el.prezzo[0]}</p>
              </a>
            </article>
          `;
          })
          .join(' ')}
      </div>
    `;

    prodottiCorrelati.innerHTML += htmlProdottiCorrelati;
  });

const handleOpzioniObbligatorie = (extraObbligo) => {
  let item = document.querySelector('input[name="nomeTitolo"]').value;
  let title = document.querySelector('.productDetails h3 strong');
  let itemPaypal = document.querySelector('input[name="item_name"]');
  title.textContent = '';
  title.textContent = `${item} - ${extraObbligo}`;
  itemPaypal.textContent = '';
  itemPaypal.textContent = `${item} - ${extraObbligo}`;
};

const handleOpzioniPiante = (pianteSelezionate) => {
  const title = document.querySelector('.productDetails h3 strong').textContent;
  let item = document.querySelector('input[name="item_name"]');
  item.value = '';
  item.value = `${title} - ${pianteSelezionate}`;
};

const handlePrice = (price, prezzo, spedizione) => {
  if (price === 'defaultPrice') {
    const title = document.querySelector('.productDetails h3 strong')
      .textContent;
    let item = document.querySelector('input[name="item_name"]');
    item.value = title;
    document.querySelectorAll('.pianteOpzioni')[0].classList.add('active');
    document.querySelectorAll('.pianteOpzioni')[1].classList.remove('active');
    document.querySelector('.price').textContent = `€ ${prezzo}`;
    document.querySelector('input[name="amount"]').value = prezzo;
    document.querySelector('input[name="shipping"]').value = spedizione;
    document.querySelector('.opzioni').style.display = 'none';
    document.querySelector('.senzaOpzioni').style.display = 'block';
  } else {
    document.querySelectorAll('.pianteOpzioni')[1].classList.add('active');
    document.querySelectorAll('.pianteOpzioni')[0].classList.remove('active');
    document.querySelector('.price').textContent = `€ ${prezzo}`;
    document.querySelector('input[name="amount"]').value = prezzo;
    document.querySelector('input[name="shipping"]').value = spedizione;
    document.querySelector('.opzioni').style.display = 'block';
    document.querySelector('.senzaOpzioni').style.display = 'none';
  }
};
