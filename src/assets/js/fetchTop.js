const topBanner = document.querySelector('#top');

fetch('https://neasy-jungle-1.herokuapp.com/top')
  .then((response) => response.json())
  .then((tops) => {
    tops.map((top, i) => {
      topBanner.style.backgroundImage = `url(${top.retro})`;
      const htmlBanner = `
                            <a data-search="${top.cerca}" href="./src/assets/html/search.html">
                                <div>
                                    <h3><strong>${top.titolo}</strong></h3>
                                    <p><strong>${top.descrizione}</strong></p>
                                </div>
                                <div>
                                    <img src="${top.foto}" />
                                </div>
                            </a>
                    `;
      topBanner.innerHTML += htmlBanner;

      const clickedProduct = document.querySelector('[data-search]');
      clickedProduct.addEventListener('click', goSearch);
    });
  })
  .catch((err) => alert(err));

const goSearch = (e) => {
  sessionStorage.setItem('clickedProduct', e.currentTarget.dataset.search);
};
