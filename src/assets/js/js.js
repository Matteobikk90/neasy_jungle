const header = document.querySelector('header');
const nav = document.querySelector('.navbarMobile');
const paths = document.querySelectorAll('path');
const burger = document.querySelector('.menu');
const clickables = document.querySelectorAll('.toggle_accordion');
const clickablesDescription = document.querySelectorAll(
  '.toggle_accordion_description'
);
const mainCategoryLinks = document.querySelectorAll('.main_category');
const mainCategoryPanels = document.querySelectorAll('.panel');
const menuCategoriesPiante = document.querySelectorAll(
  '.nj_categorie_section2 article'
);
const menuDetailsPiante = document.querySelectorAll(
  '.nj_categorie_section3_details'
);
const scrollPosition = document.querySelector('.nj_categorie_section3');
const contactForm = document.querySelector('#contactForm');
const inputsTextarea = document.querySelectorAll(
  'input[type="text"], input[type="email"], textarea'
);
const formLabels = document.querySelectorAll('#contactForm label');
const contactFormChecked = document.querySelector('#agreeContact');
const faders = document.querySelectorAll('.fade_in');
const sliders_in = document.querySelectorAll('.slide_in');

let idleTime = 0;

const width = window.width;

const scrollTop = document.querySelector('#scrollTop');

const readMoreBtns = document.querySelectorAll('.readMoreBtn');

const appearOnScroll = new IntersectionObserver(function (
  entries,
  appearOnScroll
) {
  entries.forEach((entry) => {
    if (!entry.isIntersecting) {
      return;
    } else {
      entry.target.classList.add('appear');
      appearOnScroll.unobserve(entry.target);
    }
  });
});

faders.forEach((fader) => {
  setTimeout(function () {
    appearOnScroll.observe(fader);
  }, 1000);
});

sliders_in.forEach((slider_in) => {
  setTimeout(function () {
    appearOnScroll.observe(slider_in);
  }, 500);
});

const handleSubcategories = (e) => {
  e.preventDefault();
  const panel = e.currentTarget.nextElementSibling;
  if (e.currentTarget.classList.contains('active')) {
    e.currentTarget.classList.remove('active');
    panel.style.maxHeight = null;
  } else {
    mainCategoryPanels.forEach((mainCategoryPanel) => {
      mainCategoryPanel.style.maxHeight = null;
    });
    mainCategoryLinks.forEach((mainCategoryLink) => {
      mainCategoryLink.classList.remove('active');
    });
    e.currentTarget.classList.toggle('active');
    panel.style.maxHeight = panel.scrollHeight + 'px';
  }

  setTimeout(function () {
    e.target.scrollIntoView();
  }, 500);
};

const handleScroll = () => {
  header.classList.add('scroll');
  paths.forEach((path) => path.classList.add('scroll'));
};

const handleRemoveScroll = () => {
  header.classList.remove('scroll');
  paths.forEach((path) => path.classList.remove('scroll'));
};

const checkHeader = () => {
  let scrollPosition = Math.round(window.scrollY);
  if (scrollPosition > 100) {
    handleScroll();
  } else {
    handleRemoveScroll();
  }
};

const handleClickable = (e) => {
  const { classList } = e.target.parentElement;
  let text = e.target.nextElementSibling.innerHTML;
  const mobileDescription = document.querySelector('.mobile_description');
  e.preventDefault();
  clickablesDescription.forEach((clickableDescription) => {
    clickableDescription.classList.remove('active');
  });
  classList.add('active');
  mobileDescription.innerHTML = text;
};

const handleResetActive = () => {
  mainCategoryPanels.forEach((mainCategoryPanel) => {
    mainCategoryPanel.style.maxHeight = null;
  });
};

const toggleMobileNav = () => {
  nav.classList.toggle('active');
  handleResetActive();
};

const handleReadMore = (e) => {
  e.currentTarget.classList.toggle('active');
  e.target.innerHTML =
    e.target.innerHTML === 'Leggi di più' ? 'Leggi meno' : 'Leggi di più';
};

const scrollToTop = () => {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
};

const toggleScrollTopBtn = () => {
  if (
    document.body.scrollTop > 400 ||
    document.documentElement.scrollTop > 400
  ) {
    if (scrollTop) {
      scrollTop.classList.add('active');
    }
  } else {
    if (scrollTop) {
      scrollTop.classList.remove('active');
    }
  }
};

const showDetailsPianta = (e) => {
  menuDetailsPiante.forEach((menuDetailPiante) => {
    menuDetailPiante.classList.remove('active');
  });
  let activeDetails = e.currentTarget.getAttribute('data-attribute');
  $('#' + activeDetails).addClass('active');
  window.scrollTo(scrollPosition.offsetLeft, scrollPosition.offsetTop);
};

const focusinContactForm = (e) => {
  e.currentTarget.parentElement.querySelector('label').classList.add('active');
};

const focusoutContactForm = (e) => {
  if (!e.currentTarget.value) {
    e.currentTarget.parentElement
      .querySelector('label')
      .classList.remove('active');
  }
};

const resetIdle = () => {
  idleTime = 0;
};

const timerIncrement = () => {
  idleTime = idleTime + 1;
  if (idleTime > 1) {
    if (scrollTop) {
      scrollTop.style.opacity = 0;
    }
  } else {
    if (scrollTop) {
      scrollTop.style.opacity = 1;
    }
  }
};

// This function will show the image in the lightbox
var zoomImg = function () {
  // Create evil image clone
  var clone = this.cloneNode(true);
  clone.classList.remove('zoomD');

  // Put evil clone into lightbox
  var lb = document.getElementById('lb-img');
  lb.innerHTML = '';
  lb.appendChild(clone);

  // Show lightbox
  lb = document.getElementById('lb-back');
  lb.classList.add('show');
};

window.addEventListener('load', function () {
  // Attach on click events to all .zoomD images
  var images = document.getElementsByClassName('zoomD');
  if (images.length > 0) {
    for (var img of images) {
      img.addEventListener('click', zoomImg);
    }
  }

  // Click event to hide the lightbox
  var lb = document.getElementById('lb-back');
  if (lb) {
    lb.addEventListener('click', function () {
      this.classList.remove('show');
    });
  }
});

//Increment the idle time counter every 3 seconds.

const idleInterval = setInterval(timerIncrement, 3000);

const handleContactForm = (e) => {
  e.preventDefault();
  const name = document.getElementById('name').value;
  const email = document.getElementById('email').value;
  const message = document.getElementById('message').value;

  const data = {
    name,
    email,
    message,
  };

  if (contactFormChecked.checked) {
    fetch('https://neasy-jungle-1.herokuapp.com/contactFormSubmit', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        alert('Messaggio inviato correttamente');
      })
      .catch((err) => console.log(err));
    formLabels.forEach((formLabel) => {
      formLabel.classList.remove('active');
    });
    contactForm.reset();
  } else {
    alert(
      'Please read and accept terms and conditions before sending the email'
    );
  }
};

window.addEventListener('scroll', () => {
  checkHeader(), toggleScrollTopBtn(), resetIdle();
});
//Zero the idle timer on mouse movement.
window.addEventListener('mousemove', resetIdle);
window.addEventListener('keypress', resetIdle);
//Zero the idle timer on touch events.
window.addEventListener('touchstart', resetIdle);
window.addEventListener('touchmove', resetIdle);

if (contactForm) {
  contactForm.addEventListener('submit', handleContactForm);
}

burger.addEventListener('click', toggleMobileNav);
mainCategoryLinks.forEach((mainCategoryLink) => {
  mainCategoryLink.addEventListener('click', handleSubcategories);
});
clickables.forEach((clickable) => {
  clickable.addEventListener('click', handleClickable);
});
if (scrollTop) {
  scrollTop.addEventListener('click', scrollToTop);
}

readMoreBtns.forEach((readMoreBtn) =>
  readMoreBtn.addEventListener('click', handleReadMore)
);
menuCategoriesPiante.forEach((menuCategoryPiante) => {
  menuCategoryPiante.addEventListener('click', showDetailsPianta);
});
inputsTextarea.forEach((inputTextarea) => {
  inputTextarea.addEventListener('focus', (e) => {
    focusinContactForm(e);
  });
});
inputsTextarea.forEach((inputTextarea) => {
  inputTextarea.addEventListener('blur', (e) => {
    focusoutContactForm(e);
  });
});
