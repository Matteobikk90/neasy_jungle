const loader = document.querySelector('.loaderContainer');
const evidenzaCaorusel = document.querySelector(
  '.nj_evidenza_section2 .carousel-inner'
);
const evidenzaIndicators = document.querySelector('.nj_evidenza_section2 ul');
const evidenzaBlocchi = document.querySelector('.nj_evidenza_section4');

if (loader) {
  loader.classList.add('active');
}
fetch('https://neasy-jungle-1.herokuapp.com/evidenza')
  .then((response) => response.json())
  .then((evidenzas) => {
    if (loader) {
      loader.classList.remove('active');
    }

    evidenzas.map((evidenza, i) => {
      let htmlCarousel = '';
      let htmlIndicators = '';
      if (i === 0) {
        htmlCarousel = `
                            <div class="active item carousel-item" data-slide-number="${i}">
                              <img
                                src="${evidenza.foto}"
                                class="img-fluid"
                                alt="${evidenza.titolo}" title="${evidenza.titolo}"
                              />
                              <h3><span class="marked">${evidenza.titolo}</span></h3>
                            </div>`;
      } else {
        htmlCarousel = `
                            <div class="item carousel-item" data-slide-number="${i}">
                              <img
                                src="${evidenza.foto}"
                                class="img-fluid"
                                alt="${evidenza.titolo}" title="${evidenza.titolo}"
                              />
                              <h3><span class="marked">${evidenza.titolo}</span></h3>
                            </div>`;
      }

      if (i === 0) {
        htmlIndicators = `
                          <li class="list-inline-item active">
                            <a
                              id="carousel-selector-${i}"
                              class="selected"
                              data-slide-to="${i}"
                              data-target=".dettaglio"
                            >
                              <img src="${evidenza.foto}" alt="${evidenza.titolo}" title="${evidenza.titolo}" class="img-fluid" />
                            </a>
                          </li>
                              `;
      } else {
        htmlIndicators = `
                          <li class="list-inline-item">
                            <a
                              id="carousel-selector-${i}"
                              data-slide-to="${i}"
                              data-target=".dettaglio"
                            >
                              <img src="${evidenza.foto}" class="img-fluid" alt="${evidenza.titolo}" title="${evidenza.titolo}" />
                            </a>
                          </li>
                              `;
      }

      const htmlBlocchi = `
                        <article style="background-image: url('${evidenza.retro}')">
                            <div>
                                <img alt="${evidenza.titolo}" title="${evidenza.titolo}" src="${evidenza.foto}" />
                            </div>
                            <div>
                                <h3><strong>${evidenza.titolo}</strong></h3>
                                <p><strong>${evidenza.descrizione}</strong></p>
                                <a data-search="${evidenza.cerca}" href="../html/search.html">${evidenza.titolo}</a>
                            </div>
                        </article>
                    `;
      evidenzaCaorusel.innerHTML += htmlCarousel;
      evidenzaIndicators.innerHTML += htmlIndicators;
      if (evidenzaBlocchi) {
        evidenzaBlocchi.innerHTML += htmlBlocchi;
      }

      const clickedProducts = document.querySelectorAll('[data-search]');
      if (clickedProducts) {
        clickedProducts.forEach((clickedProduct) =>
          clickedProduct.addEventListener('click', goToSearch)
        );
      }
    });
  })
  .catch((err) => alert(err));

const goToSearch = (e) => {
  sessionStorage.setItem('clickedProduct', e.currentTarget.dataset.search);
};
