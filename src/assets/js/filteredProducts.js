const loader = document.querySelector('.loaderContainer');
const searchInput = document.querySelector('input[type="text"]');
const searchResults = document.querySelector('.nj_search_section2');

const handleSearch = () => {
  const searchInputLower = searchInput.value.toLowerCase();
  searchResults.innerHTML = '';
  if (searchInputLower !== '') {
    loader.classList.add('active');
    fetch('https://neasy-jungle-1.herokuapp.com/products')
      .then((response) => response.json())
      .then((products) => {
        loader.classList.remove('active');
        const filteredProducts = products.filter((product) =>
          product.cerca.toLowerCase().includes(searchInputLower)
        );
        return filteredProducts;
      })
      .then((filteredProducts) =>
        filteredProducts.map((filteredProduct) => {
          const html = `
                          <article>
                            <a onclick="goToDetail(this.dataset.search)" href="#" data-search="${filteredProduct.titolo}"data-search="${filteredProduct.titolo}">
                              <img src="${filteredProduct.foto[0]}" />
                                <h3>
                                  <span>${filteredProduct.titolo}</span>
                                </h3>
                              <p>da &euro; ${filteredProduct.prezzo[0]}</p>
                            </a>
                          </article>
                        `;
          searchResults.innerHTML += html;
        })
      )
      .catch((err) => alert(err));
  } else {
    searchResults.innerHTML = '';
  }
};

searchInput.addEventListener('input', handleSearch);

const goToDetail = (value) => {
  sessionStorage.setItem('detailProduct', value);
  window.location.href = '../html/dettaglio.html';
};
if (sessionStorage.clickedProduct) {
  const lower = sessionStorage.clickedProduct.toLowerCase();
  searchInput.value = lower;
  handleSearch();
  sessionStorage.removeItem('clickedProduct');
}
