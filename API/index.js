const express = require('express');
const cors = require('cors');
const port = process.env.PORT || 3000;

const { db } = require('./config/db/db');

const sendMail = require('./config/mail/mail');

const app = express();

app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.get('/evidenza', async function(req, res) {
  const snapshot = await db.collection('evidenza').get();
  const docs = snapshot.docs.map(doc => doc.data());
  res.send(docs);
});

app.get('/offerteSpeciali', async function(req, res) {
  const snapshot = await db.collection('offerte').get();
  const docs = snapshot.docs.map(doc => doc.data());
  res.send(docs);
});

app.get('/top', async function(req, res) {
  const snapshot = await db.collection('banner').get();
  const docs = snapshot.docs.map(doc => doc.data());
  res.send(docs);
});

app.get('/quadriDetails', async function(req, res) {
  const snapshot = await db.collection('quadri').get();
  const docs = snapshot.docs.map(doc => doc.data());
  res.send(docs);
});

app.get('/products', (req, res) => {
  // const snapshot = await db.collection('lavagne').get();
  const getQuadri = db.collection('quadri');
  const getLavagne = db.collection('lavagne');
  const prodotti = [];

  // fetch data in parallel
  Promise.all([getQuadri.get(), getLavagne.get()])
    .then(promiseResults => {
      promiseResults.forEach(snapshot => {
        snapshot.forEach(doc => prodotti.push(doc.data()));
      });
      res.send(prodotti);
      return prodotti;
    })
    .catch(e => console.error(e));
});

app.post('/contactFormSubmit', (req, res) => {
  const { email, name, message } = req.body;
  sendMail(email, name, message, (err, data) => {
    if (err) {
      res.status(500).json({ message: 'Internal error' });
    } else {
      res.json({ message: 'Email sent' });
    }
  });
  res.json('Message received');
});

app.listen(port, () => {
  console.log(`app running on port ${port}`);
});
